package fileSystem;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystemException;
import java.nio.file.NoSuchFileException;

import org.junit.Before;
import org.junit.Test;

public class LsCommandTest {
	private Directory dir2;
	private Terminal terminal;
	private LsCommand ls;
	private FileSystem fs;

	@Before
	public void init() throws FileSystemException {
		fs = new FileSystem();
		dir2 = fs.getRoot().getSubDirectory("home");
		terminal = new Terminal(dir2, fs);
		ls = new LsCommand(terminal);
	}

	@Test
	public void Execute_EmptyCurrentDirectory_EmptyString() throws NoSuchFileException {
		String[] array = { "ls" };
		assertEquals("", ls.execute(array));
	}

	@Test
	public void execute_OnlyFiles_Files() throws NoSuchFileException {
		try {
			dir2.createFile("f1");
			dir2.createFile("f2");
		} catch (FileAlreadyExistsException e) {
			System.out.println(e.toString());
		}
		String[] array = { "ls" };
		assertEquals("f1 f2", ls.execute(array));
	}

	@Test
	public void execute_OnlyFolders_Folders() throws NoSuchFileException {
		try {
			dir2.createDirectory("d1");
			dir2.createDirectory("d2");
		} catch (FileAlreadyExistsException e) {
			System.out.println(e.toString());
		}
		String[] array = { "ls" };
		assertEquals("d1 d2", ls.execute(array));
	}

	@Test
	public void execute_FilesAndFolders_FoldersFirstAndFilesSecond() throws NoSuchFileException {
		try {
			dir2.createFile("f1");
			dir2.createFile("f2");
			dir2.createDirectory("d1");
		} catch (FileAlreadyExistsException e) {
			System.out.println(e.toString());
		}
		String[] array = { "ls" };
		assertEquals("d1 f1 f2", ls.execute(array));
	}

	@Test
	public void execute_FilesAndFoldersSortedInDescendingorder_FoldersOnFirstLineAndFilesOnSecond()
			throws FileSystemException {
		try {
			dir2.createFile("f1");
			dir2.createFile("f2");
			dir2.createDirectory("d1");
		} catch (FileAlreadyExistsException e) {
			System.out.println(e.toString());
		}
		// size=0
		dir2.write("f1", 3, "ii", false); // size=5
		dir2.write("f2", 2, "i", true);
		dir2.write("f2", 6, "", true);// size=7

		String[] array = { "ls", "--sorted", "desc" };
		assertEquals("f2 f1 d1", ls.execute(array));
	}
}
