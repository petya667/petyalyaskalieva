package fileSystem;

import static org.junit.Assert.*;

import java.nio.file.FileSystemException;

import org.junit.Before;
import org.junit.Test;

public class CdCommandTest {
	private Directory dir2;
	private Terminal terminal;
	private MkdirCommand mkdir;
	private CdCommand cd;
	private LsCommand ls;
	private FileSystem fs;

	@Before
	public void init() throws FileSystemException {
		fs = new FileSystem();
		dir2 = fs.getRoot().getSubDirectory("home");
		terminal = new Terminal(dir2, fs);
		mkdir = new MkdirCommand(terminal);
		cd = new CdCommand(terminal);
		ls = new LsCommand(terminal);
	}

	
	@Test
	public void testGivenFolderInRootDirectory() {
		fail("Not yet implemented");
	}
	@Test
	public void testGivenFolderInHomeDirectory() {
		fail("Not yet implemented");
	}
}
