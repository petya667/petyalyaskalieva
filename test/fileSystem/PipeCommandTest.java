package fileSystem;

import static org.junit.Assert.assertEquals;

import java.nio.file.FileSystemException;
import java.nio.file.NoSuchFileException;

import org.junit.Before;
import org.junit.Test;

public class PipeCommandTest {

	private Directory dir2;
	private Terminal terminal;
	private PipeCommand pipe;
	private FileSystem fs;

	@Before
	public void init() throws FileSystemException{
		fs = new FileSystem();
		dir2 = fs.getRoot().getSubDirectory("home");
		terminal = new Terminal(dir2, fs);
		pipe = new PipeCommand(terminal);
	}

	@Test
	public void testOnePipe() throws FileSystemException {
		dir2.createFile("f1");
		dir2.createFile("f2");
		dir2.write("f1", 4, "nhnh", false);
		dir2.write("f2", 1, "first line", false);
		dir2.write("f2", 3, "3 2", true);
		assertEquals("1 f1\n4 f2", pipe.execute(new String[] { "ls", "|", "wc" }));
	}

	@Test
	public void testOnePipe_TheCommandAfterThePipeWithOptions() throws FileSystemException {
		dir2.createFile("f1");
		dir2.createFile("f2");
		dir2.write("f1", 4, "nhnh", false);
		dir2.write("f2", 1, "first line", false);
		dir2.write("f2", 3, "3 2", true);
		assertEquals("4 f1\n3 f2", pipe.execute(new String[] { "ls", "|", "wc","-l" }));
	}
	
	@Test
	public void testMultiplePipes_Text() throws FileSystemException {
		dir2.createFile("f2");
		dir2.write("f2", 1, "first line", false);
		dir2.write("f2", 3, "3 2", true);
		assertEquals("4", pipe.execute(new String[] { "ls","|","cat", "|", "wc" }));
	}

	@Test(expected = NoSuchFileException.class)
	public void testWrongCommandBeforePipe() throws FileSystemException {
		assertEquals("4", pipe.execute(new String[] { "create_file","f1","|", "wc" }));
	}
}
