package fileSystem;

import static org.junit.Assert.*;

import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystemException;

import org.junit.Before;
import org.junit.Test;

public class DirectoryTest {
	private Directory dir;
	private String result;

	@Before
	public void init() {
		dir = new Directory("dir");
		try {
			dir.createFile("file1");
			dir.createFile("file2");
			dir.createFile("file3");
		} catch (FileAlreadyExistsException e) {
			System.out.println(e.toString());
		}
	}

	@Test
	public void listBySize_Descending_WithFiles() throws FileSystemException {
		dir.write("file1", 2, "jjj", true); // middle
		dir.write("file2", 1, "111", true); // smaller size
		dir.write("file3", 2, "jjjnnn", true); // biggest size
		result = new String("file3 file1 file2"); // 3,1,2
		assertEquals(result, dir.listBySize_Descending());
	}

	@Test
	public void listBySize_Descending_WithFilesAndFolders() throws FileSystemException {
		dir.write("file1", 2, "jjj", true);
		dir.write("file2", 1, "1", true);
		dir.write("file3", 2, "jjjnnn", true);
		try {
			dir.createDirectory("dir1");
			dir.createDirectory("dir2");
		} catch (FileAlreadyExistsException e) {
			System.out.println(e.toString());
		}
		result = new String("file3 file1 file2 dir1 dir2");
		assertEquals(result, dir.listBySize_Descending());
	}

	@Test
	public void list_FoldersAndFilesInOneString() {
		try {
			dir.createDirectory("dir1");
			dir.createDirectory("dir2");
		} catch (FileAlreadyExistsException e) {
			System.out.println(e.toString());
		}
		String result = new String("dir2 dir1 file2 file3 file1"); 
		assertEquals(result, dir.list());
	}
}
