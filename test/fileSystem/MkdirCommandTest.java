package fileSystem;

import static org.junit.Assert.assertEquals;

import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystemException;
import java.nio.file.NoSuchFileException;

import org.junit.Before;
import org.junit.Test;

public class MkdirCommandTest {
	private Directory dir2;
	private Terminal terminal;
	private MkdirCommand mkdir;
	private LsCommand ls;
	private FileSystem fs;

	@Before
	public void init() throws FileSystemException {
		fs = new FileSystem();
		dir2 = fs.getRoot().getSubDirectory("home");
		terminal = new Terminal(dir2, fs);
		mkdir = new MkdirCommand(terminal);
		ls = new LsCommand(terminal);
	}

	@Test
	public void execute_CreateDirectories() throws FileSystemException {
		mkdir.execute(new String[] { "mkdir", "dir1", "dir2" });
		assertEquals("dir2 dir1", ls.execute(new String[] { "ls" }));
	}

	@Test(expected = FileAlreadyExistsException.class)
	public void execute_CreateDirectoriesWithDuplicatingNames_FileAlreadyExistsException() throws FileSystemException {
		mkdir.execute(new String[] { "mkdir", "dir1" });
		mkdir.execute(new String[] { "mkdir", "dir1" });
	}

	@Test
	public void testWithGivenAbsolutePath() throws FileSystemException {
		mkdir.execute(new String[] { "mkdir", "/home/d1", "dir2" });
		assertEquals("dir2 d1", ls.execute(new String[] { "ls" }));
		mkdir.execute(new String[] { "mkdir", "/home/d1/d3" });
		CdCommand cd = new CdCommand(terminal);
		cd.execute(new String[] { "cd", "/home/d1" });
		terminal.getCurrent().list();
		assertEquals("d3", ls.execute(new String[] { "ls" }));
		cd.execute(new String[] { "cd", "/home/d1/d3" }); //
		mkdir.execute(new String[] { "mkdir", "newdir" });
		assertEquals("newdir", ls.execute(new String[] { "ls" }));
	}

	@Test
	public void testWithGivenRelativePath() throws FileSystemException {
		mkdir.execute(new String[] { "mkdir", "../home/d1", "dir2" });
		assertEquals("dir2 d1", ls.execute(new String[] { "ls" }));
		mkdir.execute(new String[] { "mkdir", "./d1/d3" });
		CdCommand cd = new CdCommand(terminal);
		cd.execute(new String[] { "cd", "../home/d1" });
		terminal.getCurrent().list();
		assertEquals("d3", ls.execute(new String[] { "ls" }));
		cd.execute(new String[] { "cd", "./d3" });
		mkdir.execute(new String[] { "mkdir", ".././newdir" });
		cd.execute(new String[] { "cd", ".." }); // in d2
		assertEquals("newdir d3", ls.execute(new String[] { "ls" }));
	}

	@Test(expected = NoSuchFileException.class)
	public void execute_PassAbsolutePathWithNoExistingFolders_NoSuchFileException() throws FileSystemException {
		mkdir.execute(new String[] { "mkdir", "dir1", "dir2" });
		mkdir.execute(new String[] { "mkdir", "/home/dir2/nonexisting/newfolder" });
	}

	@Test(expected = NoSuchFileException.class)
	public void execute_PassRelativePathWithNoExistingFolders_NoSuchFileException() throws FileSystemException {
		mkdir.execute(new String[] { "mkdir", "dir1", "dir2" });
		new CdCommand(terminal).execute(new String[] { "cd", "dir2" });
		mkdir.execute(new String[] { "mkdir", "./nonexisting/newfolder" });
	}
}
