package fileSystem;

import org.junit.Test;
import static org.junit.Assert.*;

import java.nio.file.FileSystemException;

import org.junit.Before;

public class FileTest {
	private File obj;

	@Before
	public void init() {
		obj = new File();
	}

	@Test
	public void Write_WritingOnNewLine_Text() throws FileSystemException {
		obj.write(2, "hh ll", true);
		String[] array = { "", "hh ll" };
		assertArrayEquals(array, obj.open());
	}

	@Test
	public void Write_WritingOnExistingLine_Text() throws FileSystemException {
		obj.write(2, "hh ll", true);
		obj.write(2, "new", true);
		obj.write(2, " text", false);
		obj.write(1, "first", true);
		String[] array = { "first", "new text" };
		assertArrayEquals(array, obj.open());
	}

}
