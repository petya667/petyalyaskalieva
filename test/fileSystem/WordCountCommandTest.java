package fileSystem;

import static org.junit.Assert.*;

import java.nio.file.FileSystemException;

import org.junit.Before;
import org.junit.Test;

public class WordCountCommandTest {
	private Directory dir2;
	private Terminal terminal;
	private WordCountCommand wc;
	private FileSystem fs;

	@Before
	public void init() throws FileSystemException {
		fs = new FileSystem();
		dir2 = fs.getRoot().getSubDirectory("home");
		terminal = new Terminal(dir2, fs);
		wc = new WordCountCommand(terminal);
	}

	@Test
	public void testWithFewFilesPassed() throws FileSystemException {
		dir2.createFile("f1");
		dir2.createFile("f2");
		dir2.write("f1", 4, "nhnh", false);
		dir2.write("f2", 1, "first line", false);
		dir2.write("f2", 3, "3 2", true);
		assertEquals("1 f1\n4 f2", wc.execute(new String[] { "wc", "f1", "f2" }));
	}

	@Test
	public void testWithFewFilesPassed_GetNumberOfLines() throws FileSystemException {
		dir2.createFile("f1");
		dir2.createFile("f2");
		dir2.write("f1", 4, "nhnh", false);
		dir2.write("f2", 1, "first line", false);
		dir2.write("f2", 3, "3 2", true);
		assertEquals("4 f1\n3 f2", wc.execute(new String[] { "wc", "-l", "f1", "f2" }));
	}

}
