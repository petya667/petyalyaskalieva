package fileSystem;

import static org.junit.jupiter.api.Assertions.*;

import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystemException;
import java.nio.file.NoSuchFileException;

import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.BeforeEach;

class CatCommandTest { // in junit5 
	private Directory dir2;
	private Terminal terminal;
	private CatCommand cat;
	private FileSystem fs;

	@BeforeEach
	public void init() throws FileSystemException {
		fs = new FileSystem();
		dir2 = fs.getRoot().getSubDirectory("home");
		terminal = new Terminal(dir2, fs);
		cat = new CatCommand(terminal);
		try {
			dir2.createFile("f1");
		} catch (FileAlreadyExistsException e) {
			System.out.println(e.toString());
		}
	}

	@Test
	public void execute_EmptyFile_EmptyString() throws FileSystemException {
		String[] array = { "cat", "f1" };
		try {
			assertEquals("", cat.execute(array));
		} catch (NoSuchFileException e) {
			System.out.println(e.toString());
		}
	}

	@Test
	public void execute_File_FileContent() throws FileSystemException {
		dir2.write("f1", 3, "jnjnj jnj jn", false);
		String[] array = { "cat", "f1" };
		try {
			assertEquals("\n\njnjnj jnj jn", cat.execute(array));
		} catch (NoSuchFileException e) {
			System.out.println(e.toString());
		}
	}

	@Test
	public void execute_FileWithOverwrite_FileContent() throws FileSystemException {
		dir2.write("f1", 3, "jnjnj jnj jn", false);
		dir2.write("f1", 3, "new content", true);
		dir2.write("f1", 2, "second line", false); // its a new line
		dir2.write("f1", 4, "4", true);
		String[] array = { "cat", "f1" };
		try {
			assertEquals("\nsecond line\nnew content\n4", cat.execute(array));
		} catch (NoSuchFileException e) {
			System.out.println(e.toString());
		}
	}

	@Test
	public void execute_NonExistingFileName_NoSuchFileException() {

		String[] array = { "cat", "f5" };
		assertThrows(NoSuchFileException.class, () -> {  //in junit5
			cat.execute(array);
		});
	}
}
