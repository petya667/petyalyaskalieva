package fileSystem;

import static org.junit.Assert.*;

import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystemException;

import org.junit.Before;
import org.junit.Test;

public class CreateFileCommandTest {
	private Directory dir2;
	private Terminal terminal;
	private LsCommand ls;
	private CreateFileCommand createFile;
	private FileSystem fs;

	@Before
	public void init() throws FileSystemException {
		fs = new FileSystem();
		dir2 = fs.getRoot().getSubDirectory("home");
		terminal = new Terminal(dir2, fs);
		createFile = new CreateFileCommand(terminal);
		ls = new LsCommand(terminal);
	}

	@Test(expected = FileAlreadyExistsException.class)
	public void execute_CreateFileWithAlreadyExistingName_FileAlreadyExistsException() throws FileSystemException {
		String[] input = { "create_file", "f1" };
		createFile.execute(input);
		String[] input1 = { "create_file", "f1" };
		createFile.execute(input1);
	}

	@Test
	public void execute_CreateFiles() throws FileSystemException {
		String[] input = { "create_file", "f1" };
		createFile.execute(input);
		String[] input1 = { "create_file", "f2" };
		createFile.execute(input1);
		String[] inputLs = { "ls" };
		assertEquals("f1 f2", ls.execute(inputLs));
	}

	@Test
	public void execute_CreateFilesWithOneCommand() throws FileSystemException {
		String[] input = { "create_file", "f1", "f2", "f3" };
		createFile.execute(input);
		String[] inputLs = { "ls" };
		assertEquals("f1 f2 f3", ls.execute(inputLs));
	}

}
