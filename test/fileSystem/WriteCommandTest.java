package fileSystem;

import static org.junit.Assert.assertEquals;

import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystemException;
import java.nio.file.NoSuchFileException;

import org.junit.Before;
import org.junit.Test;

public class WriteCommandTest {
	private Directory dir2;
	private Terminal terminal;
	private WriteCommand obj;
	private CatCommand cat;
	private FileSystem fs;

	@Before
	public void init() throws FileSystemException {
		fs = new FileSystem();
		dir2 = fs.getRoot().getSubDirectory("home");
		terminal = new Terminal(dir2, fs);
		obj = new WriteCommand(terminal);
		cat = new CatCommand(terminal);
		try {
			dir2.createFile("f1");
		} catch (FileAlreadyExistsException e) {
			System.out.println(e.toString());
		}
	}

	@Test
	public void execute_WritingOnFewLinesWithoutOverwrite_Text() throws FileSystemException {
		obj.execute(new String[] { "write", "f1", "2", "second", "line" });
		obj.execute(new String[] { "write", "f1", "4", "four" });
		obj.execute(new String[] { "write", "f1", "2", "new", "content" });
		String result = new String("\nsecond linenew content\n\nfour");
		assertEquals(result, cat.execute(new String[] { "cat", "f1" }));
	}

	@Test
	public void execute_WritingOnFewLinesWithOverwrite_Text() throws FileSystemException {
		obj.execute(new String[] { "write", "f1", "2", "second", "line" });
		obj.execute(new String[] { "write", "f1", "5", "5" });
		obj.execute(new String[] { "write", "f1", "2", "new", "content", "-overwrite" });
		String result = new String("\nnew content\n\n\n5");
		assertEquals(result, cat.execute(new String[] { "cat", "f1" }));

	}

	@Test(expected = NoSuchFileException.class)
	public void execute_NonExistingFileName_NoSuchFileException() throws FileSystemException {
		obj.execute(new String[] { "write", "f5", "2", "second", "line" });
	}
}
