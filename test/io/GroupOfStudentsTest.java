package io;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import io.Student.Gender;

public class GroupOfStudentsTest {
	private Student s1;
	private Student s2;
	private Student s3;
	private Student s4;
	private Student s5;
	List<Student> list;
	GroupOfStudents group;

	@Before
	public void init() {
		s1 = new Student("Ivan", 4.5, "ivan@abv.bg", Gender.MALE, 13);
		s2 = new Student("Iva", 4.0, "iva19@abv.bg", Gender.FEMALE, 15);
		s3 = new Student("Yana", 6.0, "yana@abv.bg", Gender.FEMALE, 15);
		s4 = new Student("Elena", 2.0, "elena9@abv.bg", Gender.FEMALE, 13);
		s5 = new Student("Atanas", 5.5, "atanas@abv.bg", Gender.MALE, 15);
		list = Arrays.asList(s1, s2, s3, s4, s5);
		group = new GroupOfStudents();
	}

	@Test
	public void testGetAverageMarkWithFewStudents() {
		group.addListOfStudents(list);
		assertEquals(4.4, group.getAverageMark(),0.1);
	}

	@Test
	public void testGetAverageMarkWithZeroStudents() {
		assertEquals(0.0, group.getAverageMark(),0.1);
	}
	
	@Test
	public void testgetAllPassingStudents() {
		group.addListOfStudents(list);
		List<Student> result = Arrays.asList(s1,s2,s3,s5);
		assertEquals(result,group.getAllPassingStudents());
	}
	
	@Test
	public void testGetAllFailingStudents() {
		group.addListOfStudents(list);
		List<Student> result = Arrays.asList(s4);
		assertEquals(result,group.getAllFailingStudents());
	}
	
	@Test
	public void testSplitbyMark2_OneEmptyGroupAndOneWithAllStudents() {
		group.addListOfStudents(list);
		List<Student> greater = Arrays.asList(s1,s2,s3,s4,s5);
		List<Student> low = Arrays.asList();
		Map<Boolean, List<Student>> result = new HashMap<>();
		result.put(true, greater);
		result.put(false,low);
		assertEquals(result,group.splitbyMarks(2.0));
	}
	
	@Test
	public void testSplitbyMark4point5_TwoGroupsWithStudents() {
		group.addListOfStudents(list);
		List<Student> greater = Arrays.asList(s1,s3,s5);
		List<Student> low = Arrays.asList(s2,s4);
		Map<Boolean, List<Student>> result = new HashMap<>();
		result.put(true, greater);
		result.put(false,low);
		assertEquals(result,group.splitbyMarks(4.5));
	}
	
	@Test
	public void testOrderByMarksAscending() {
		group.addListOfStudents(list);
		List<Student> result = Arrays.asList(s4,s2,s1,s5,s3);
		assertEquals(result,group.orderByMarksAscending());
	}
	
	@Test
	public void testOrderByMarksDescending() {
		group.addListOfStudents(list);
		List<Student> result = Arrays.asList(s3,s5,s1,s2,s4);
		assertEquals(result,group.orderByMarksDescending());
	}
	           
	@Test
	public void testClusterizeAndReturnStudentsWithHighestMarks() {
		group.addListOfStudents(list);
		List<Student> result = Arrays.asList(s3);
		assertEquals(result,group.clusterizeAndReturnStudentsWithHighestMarks());
	}
	
	@Test
	public void testClusterizeAndReturnStudentsWithLowestMarks() {
		group.addListOfStudents(list);
		List<Student> result = Arrays.asList(s4);
		assertEquals(result,group.clusterizeAndReturnStudentsWithLowestMarks());
	}
	
	@Test
	public void testGetMarksDistributionByAge() {
		group.addListOfStudents(list);
		List<Double> age13 = Arrays.asList(4.5,2.0);
		List<Double> age15 = Arrays.asList(4.0,6.0,5.5);
		Map<Integer,List<Double>> result = new HashMap<>();
		result.put(13,age13);
		result.put(15, age15);
		assertEquals(result,group.getMarksDistributionByAge());
	}
	
	@Test
	public void testGetAverageMarkByGender_Female() {
		group.addListOfStudents(list);
		assertEquals(4.0, group.getAverageMarkByGender(Gender.FEMALE),0.1);
	}
	
	@Test
	public void testGetAverageMarkByGender_Male() {
		group.addListOfStudents(list);
		assertEquals(5.0, group.getAverageMarkByGender(Gender.MALE),0.1);
	}
	
	@Test
	public void testGetMarksByDistribution() {  //4.5  4.0  6.0  2.0  5.5
		group.addListOfStudents(list);
		List<Student> two = Arrays.asList(s4);
		List<Student>  four= Arrays.asList(s2);
		List<Student>  fourpoint5= Arrays.asList(s1);
		List<Student>  fivepoint5= Arrays.asList(s5);
		List<Student>  six= Arrays.asList(s3);
		Map<Double, List<Student>> result = new HashMap<>();
		result.put(2.0, two);
		result.put(4.0,four);
		result.put(4.5,fourpoint5);
		result.put(5.5,fivepoint5);
		result.put(6.0,six);
		assertEquals(result,group.getMarksByDistribution());
	}
	
	@Test
	public void testGetEmail() {
		group.addListOfStudents(list);
		List<String> result = Arrays.asList("ivan@abv.bg","iva19@abv.bg","yana@abv.bg","elena9@abv.bg","atanas@abv.bg");
		assertEquals(result,group.getEmail());
	}
	
	@Test
	public void testSplitByGenderAndPartitionByAge() {  
		group.addListOfStudents(list);
		List<Student> female_13 = Arrays.asList(s4);
		List<Student>  female_15= Arrays.asList(s2,s3);
		List<Student>  male_13= Arrays.asList(s1);
		List<Student>  male_15= Arrays.asList(s5);
		Map<Integer,List<Student>> innerMapFemale = new HashMap<>();
		Map<Integer,List<Student>> innerMapMale = new HashMap<>();
		Map<Gender, Map<Integer,List<Student>>> result = new HashMap<>();
		innerMapFemale.put(13, female_13);
		innerMapFemale.put(15,female_15);
		innerMapMale.put(13, male_13);
		innerMapMale.put(15,male_15);
		result.put(Gender.FEMALE,innerMapFemale);
		result.put(Gender.MALE,innerMapMale);
		assertEquals(result,group.splitByGenderAndPartitionByAge());
	}
	
	
	
	
	
	
}
