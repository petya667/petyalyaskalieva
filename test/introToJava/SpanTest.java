package introToJava;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import introToJava.Span;

class SpanTest {

	@Test
	void MaxSpan_NoRepeatingDigits_Zero() {
		Span obj = new Span();
		int[] a= {1,2,3,4,5,6};
	   assertEquals(0,obj.maxSpan(a));
	}
	@Test
	void MaxSpan_Array_MaxSpan() {
		Span obj = new Span();
		int[] a= {1,2,3,4,5,6,1};
	   assertEquals(7,obj.maxSpan(a));
	}
}
