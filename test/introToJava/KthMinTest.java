package introToJava;

import static org.junit.Assert.*;

import org.junit.Test;

import introToJava.KthMin;

public class KthMinTest {

	@Test
	public void KthMin_NullArray_NULL() {
		KthMin obj = new KthMin();
		int[] a = { 0, 0 };
		assertEquals(0, obj.kthMin(2, a));
	}

	@Test
	public void KthMin_Array_True() {
		KthMin obj = new KthMin();
		int[] a = { 0, 5, 7, 8, 2 };
		assertEquals(2, obj.kthMin(2, a));
	}

	@Test
	public void KthMin_Array_False() {
		KthMin obj = new KthMin();
		int[] a = { 5, 2, 2, 3, -1, -6 };
		assertNotEquals(3, obj.kthMin(2, a));
	}
}
