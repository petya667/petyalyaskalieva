package introToJava;

import static org.junit.Assert.*;

import org.junit.Test;

import introToJava.OddTimes;

public class OddTimesTest {

	@Test
	public void GetOddOccurrence_NullArray() {
		OddTimes obj = new OddTimes();
		int[] a = { 0, 0 };
		assertEquals("no matches", -1, obj.getOddOccurrence(a));
	}

	@Test
	public void GetOddOccurrence_Array_False() {
		OddTimes obj = new OddTimes();
		int[] a = { 0, 1, 3, 5, 6, 3, 3 };
		assertNotEquals(3, obj.getOddOccurrence(a));
	}

	@Test
	public void GetOddOccurrence_Array_True() {
		OddTimes obj = new OddTimes();
		int[] a = { 0, 1, 1, 2, 0, 5 };
		assertEquals(2, obj.getOddOccurrence(a));
	}
}
