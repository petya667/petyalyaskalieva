package introToJava;

import static org.junit.Assert.*;

import org.junit.Test;

import introToJava.MinElement;

public class MinElementTest {

	@Test
	public void Min_NullArray_NULL() {
		MinElement obj = new MinElement();
		int[] a = { 0, 0 };
		assertEquals(0, obj.min(a));
	}

	@Test
	public void Min_Array_True() {
		MinElement obj = new MinElement();
		int[] a = { 0, -1, 5, 6, -50 };
		assertEquals(-50, obj.min(a));
	}

	@Test
	public void Min_Array_False() {
		MinElement obj = new MinElement();
		int[] a = { 0, 60, 2, -7 };
		assertNotEquals(60, obj.min(a));
	}
}
