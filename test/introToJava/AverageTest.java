package introToJava;

import static org.junit.Assert.*;

import org.junit.Test;

import introToJava.Average;

public class AverageTest {

	@Test
	public void GetAverage_NullArray_NULL() {
		Average obj = new Average();
		int[] a = { 0, 0 };
		assertEquals(0, obj.getAverage(a));
	}

	@Test
	public void GetAverage_Array_True() {
		Average obj = new Average();
		int[] a = { 1, 1, 1 };
		assertEquals(1, obj.getAverage(a));
	}

	@Test
	public void testGetAverage_Array_False() {
		Average obj = new Average();
		int[] a = { 8, 8, 31, 4 };
		assertNotEquals(0, obj.getAverage(a));
	}
}
