package introToJava;

import static org.junit.Assert.*;

import org.junit.Test;

import introToJava.Decode;

public class DecodeTest {

	@Test
	public void decodeUrl_urlString_DecodedString() {
		String input = "%20ngjjd%3A";
		Decode obj = new Decode();
		assertEquals(" ngjjd:", obj.decodeUrl(input));
	}

	@Test
	public void decodeUrl_urlString_DecodedString_Test2() {
		String input = "%%%%ngjjd%3A%3";
		Decode obj = new Decode();
		assertEquals("%%%%ngjjd:%3", obj.decodeUrl(input));
	}
}
