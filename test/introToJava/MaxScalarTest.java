package introToJava;

import static org.junit.Assert.*;

import org.junit.Test;

import introToJava.MaxScalar;

public class MaxScalarTest {

	@Test
	public void MaximalScalarSum_Arrays_MaxScalar() {
		MaxScalar obj = new MaxScalar();
		int a[] = new int[3];
		int b[] = new int[3];
		a[0] = 4;
		a[1] = 1;
		a[2] = 5;
		b[0] = 3;
		b[1] = 7;
		b[2] = 10;
		assertEquals(30, obj.maximalScalarSum(a, b));
	}

	@Test
	public void MaximalScalarSum_ArraysWith4Coordinates_MaxScalar() {
		MaxScalar obj = new MaxScalar();
		int a[] = new int[4];
		int b[] = new int[4];
		a[0] = 3;
		a[1] = 1;
		a[2] = 5;
		a[3] = 7;
		b[0] = 1;
		b[1] = 7;
		b[2] = 10;
		b[3] = 8;
		assertEquals(42, obj.maximalScalarSum(a, b));
	}

	@Test
	public void MaximalScalarSum_ArraysWith1Coordinate_MaxScalar() {
		MaxScalar obj = new MaxScalar();
		int a[] = new int[1];
		int b[] = new int[1];
		a[0] = 4;
		b[0] = 3;
		assertEquals(7, obj.maximalScalarSum(a, b));
	}
}
