package introToJava;

import static org.junit.Assert.*;

import org.junit.Test;

import introToJava.Reverse;

public class ReverseTest {

	@Test
	public void Reverse_Char_Char() {
		Reverse obj = new Reverse();
		assertEquals("a", obj.reverse("a"));
	}

	@Test
	public void Reverse_String_ReversedString() {
		Reverse obj = new Reverse();
		assertEquals("hello", obj.reverse("olleh"));
	}
}
