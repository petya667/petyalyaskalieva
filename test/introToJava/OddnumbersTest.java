package introToJava;

import static org.junit.Assert.*;

import org.junit.Test;

import introToJava.Oddnumbers;

public class OddnumbersTest {

	@Test
	public void IsOdd_Null_False() {
		Oddnumbers obj = new Oddnumbers();
		assertFalse(obj.isOdd(0));
	}

	@Test
	public void IsOdd_OddNumber_True() {
		Oddnumbers obj = new Oddnumbers();
		assertTrue(obj.isOdd(3));
	}

	@Test
	public void IsOdd_EvenNumber_False() {
		Oddnumbers obj = new Oddnumbers();
		assertFalse(obj.isOdd(2));
	}
}
