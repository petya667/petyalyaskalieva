package introToJava;

import static org.junit.Assert.*;

import org.junit.Test;

import introToJava.Prime;

public class PrimeTest {

	@Test
	public void testIsPrimeNULL() {
		assertFalse(Prime.isPrime(0));
	}

	@Test
	public void testIsPrimeNotPrime() {
		assertFalse(Prime.isPrime(4));
	}

	@Test
	public void testIsPrimeTrue() {
		assertTrue(Prime.isPrime(2));
	}
}
