package introToJava;

import static org.junit.Assert.*;

import org.junit.Test;

import introToJava.LargestPalindrome;

public class LargestPalindromeTest {

	@Test
	public void IsPalindrome_1Digit_True() {
		LargestPalindrome obj = new LargestPalindrome();
		assertTrue(obj.isPalindrome(7));
	}

	@Test
	public void IsPalindrome_2Digits_True() {
		LargestPalindrome obj = new LargestPalindrome();
		assertTrue(obj.isPalindrome(99));
	}

	@Test
	public void IsPalindrome_2Digits_False() {
		LargestPalindrome obj = new LargestPalindrome();
		assertFalse(obj.isPalindrome(91));
	}

	@Test
	public void IsPalindrome_3Digits_True() {
		LargestPalindrome obj = new LargestPalindrome();
		assertTrue(obj.isPalindrome(919));
	}

	@Test
	public void GetLargestPalindrome_1Digit_EqualsToTheDigit() {
		LargestPalindrome obj = new LargestPalindrome();
		assertEquals(6, obj.getLargestPalindrome(6));
	}

	@Test
	public void GetLargestPalindrome_2Digits_LargestPalindrome() {
		LargestPalindrome obj = new LargestPalindrome();
		assertEquals(55, obj.getLargestPalindrome(57));
	}

	@Test
	public void GetLargestPalindrome_3Digits_LargestPalindrome() {
		LargestPalindrome obj = new LargestPalindrome();
		assertEquals(626, obj.getLargestPalindrome(626));
	}
}
