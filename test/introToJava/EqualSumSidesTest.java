package introToJava;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import introToJava.EqualSumSides;

class EqualSumSidesTest {

	@Test
	void EqualSumSides1_Array_True() {
		int a[] = { 3, 0, -1, 2, 1 };
		EqualSumSides obj = new EqualSumSides();
		assertTrue(obj.equalSumSides1(a));
	}

	@Test
	void EqualSumSides1_Array_False() {
		int a[] = { 100, 100 };
		EqualSumSides obj = new EqualSumSides();
		assertFalse(obj.equalSumSides1(a));
	}
}
