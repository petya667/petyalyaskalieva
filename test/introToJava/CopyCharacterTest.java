package introToJava;

import static org.junit.Assert.*;

import org.junit.Test;

import introToJava.CopyCharacter;

public class CopyCharacterTest {

	@Test
	public void copyChars_String_Stringx4() {
		CopyCharacter obj = new CopyCharacter();
		assertEquals("aaaaaaaa", obj.copyChars("aa", 4));
	}

	@Test
	public void copyChars_String_False() {
		CopyCharacter obj = new CopyCharacter();
		assertNotEquals("ababb", obj.copyChars("ab", 2));
	}
}
