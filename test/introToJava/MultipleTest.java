package introToJava;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import introToJava.Multiple;

class MultipleTest {

	@Test
	public void testIsPrimeNULL() {
		assertFalse(Multiple.isPrime(0));
	}

	@Test
	public void testIsPrimeNotPrime() {
		assertFalse(Multiple.isPrime(4));
	}

	@Test
	public void testIsPrimeTrue() {
		assertTrue(Multiple.isPrime(2));
	}

	@Test
	void testGetSmallestMultipleTrue() {
		Multiple obj = new Multiple();
		assertEquals(60, obj.getSmallestMultiple(6));
	}

	@Test
	void testGetSmallestMultiplFalse() {
		Multiple obj = new Multiple();
		assertNotEquals(12, obj.getSmallestMultiple(3));
	}
}
