package introToJava;

import static org.junit.Assert.*;

import org.junit.Test;

import introToJava.Palindrome;

public class PalindromeTest {

	@Test
	public void IsPalindrome_String_True() {
		Palindrome obj = new Palindrome();
		assertTrue(obj.isPalindrome("abcdcba"));
	}

	@Test
	public void IsPalindrome_Char_True() {
		Palindrome obj = new Palindrome();
		assertTrue(obj.isPalindrome("a"));
	}

	@Test
	public void IsPalindrome_String_False() {
		Palindrome obj = new Palindrome();
		assertFalse(obj.isPalindrome("abcdcbaff"));
	}
}
