package introToJava;

import static org.junit.Assert.*;

import org.junit.Test;

import introToJava.DoubleFactorial;

public class DoubleFactorialTest {

	@Test
	public void fac_Null_Equals1() {
		DoubleFactorial obj = new DoubleFactorial();
		assertEquals(1, obj.fac(0L));
	}

	@Test
	public void doubleFac_Null_Equals1() {
		DoubleFactorial obj = new DoubleFactorial();
		assertEquals(1, obj.doubleFac(0L));
	}

	@Test
	public void ifNotEqualsThrowsException() {
		DoubleFactorial obj = new DoubleFactorial();
		assertEquals(6, obj.fac(3));
	}

	@Test
	public void ifNotEqualsThrowsException2() {
		DoubleFactorial obj = new DoubleFactorial();
		assertEquals(720, obj.doubleFac(3));
	}

}
