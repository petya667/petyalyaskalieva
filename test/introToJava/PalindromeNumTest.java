package introToJava;

import static org.junit.Assert.*;

import org.junit.Test;

import introToJava.PalindromeNum;

public class PalindromeNumTest {

	@Test
	public void IsPalindrome_1Digit_True() {
		PalindromeNum obj = new PalindromeNum();
		assertTrue(obj.isPalindrome(0));
	}

	@Test
	public void IsPalindrome_3Digit_True() {
		PalindromeNum obj = new PalindromeNum();
		assertTrue(obj.isPalindrome(999));
	}

	@Test
	public void IsPalindrome_2Digits_False() {
		PalindromeNum obj = new PalindromeNum();
		assertFalse(obj.isPalindrome(51));
	}
}
