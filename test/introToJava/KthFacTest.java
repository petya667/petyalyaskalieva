package introToJava;

import static org.junit.Assert.*;

import org.junit.Test;

import introToJava.KthFac;

public class KthFacTest {

	@Test
	public void kthFac_Null_Equals1() {
		KthFac obj = new KthFac();
		assertEquals(1, obj.kthFac(0, 5));
	}

	@Test
	public void kthFac_Number_KthFacOfNum() {
		KthFac obj = new KthFac();
		assertEquals(720, obj.kthFac(2, 3));
	}
}
