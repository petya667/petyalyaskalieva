package introToJava;

import static org.junit.Assert.*;

import org.junit.Test;

import introToJava.Mentions;

public class MentionsTest {

	@Test
	public void Mentions_Strings_ZeroMentions() {
		Mentions obj = new Mentions();
		assertEquals(0,obj.mentions("mkjhb", "lllll"));
	}

	@Test
	public void Mentions_Strings_NumberOfMentions() {
		Mentions obj = new Mentions();
		assertEquals(4,obj.mentions("what", "whattfwahtfwhatawhathwatwhat"));
	}
	@Test
	public void Mentions_Strings_NumberOfMentions_Test2() {
		Mentions obj = new Mentions();
		assertEquals(3,obj.mentions("what", "whatwhatwhat"));
	}
}
