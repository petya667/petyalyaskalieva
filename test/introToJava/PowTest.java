package introToJava;

import static org.junit.Assert.*;

import org.junit.Test;

import introToJava.Pow;

public class PowTest {

	@Test
	public void Pow_SecondArgumentIsZero_1() {
		Pow obj = new Pow();
		assertEquals(1, obj.pow(2, 0));
	}

	@Test
	public void Pow_True() {
		Pow obj = new Pow();
		assertEquals(4, obj.pow(2, 2));
	}

	@Test
	public void Pow_False() {
		Pow obj = new Pow();
		assertNotEquals(128, obj.pow(2, 10));
	}

}
