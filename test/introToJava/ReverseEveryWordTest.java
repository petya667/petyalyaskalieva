package introToJava;

import static org.junit.Assert.*;

import org.junit.Test;

import introToJava.ReverseEveryWord;

public class ReverseEveryWordTest {

	@Test
	public void ReverseEveryWord_1Word_ReversedWord() {
		ReverseEveryWord obj = new ReverseEveryWord();
		assertEquals("olleh", obj.reverseEveryWord("hello"));
	}

	@Test
	public void ReverseEveryWord_Text_ReversedEveryWord() {
		ReverseEveryWord obj = new ReverseEveryWord();
		assertEquals("siht si tahW  ", obj.reverseEveryWord("  What is this"));
	}
}
