package dataStructuresAndAlgorithmsInJava;

import static org.junit.Assert.*;

import org.junit.Test;

import dataStructuresAndAlgorithmsInJava.LinkedList;

public class LinkedListTest {

	@Test
	public void Add_DataToTheFirstPosition_True() {
		LinkedList obj = new LinkedList();
		assertTrue(obj.add(0, 77));
	}

	@Test
	public void Add_DataToSomePosition_False() {
		LinkedList obj = new LinkedList();
		assertFalse(obj.add(5, 77));
	}

	@Test
	public void Add_DataToSomePosition_True() {
		LinkedList obj = new LinkedList();
		assertTrue(obj.add(0, 77));
		assertTrue(obj.add(1, 3));
		assertTrue(obj.add(2, 88));
		assertTrue(obj.add(1, 88));
	}

	@Test
	public void Get_FirstAndThirdPosition_TheData() {
		LinkedList obj = new LinkedList();
		obj.add(0, 77);
		obj.add(1, 3);
		obj.add(2, 88);
		assertEquals(77, obj.get(0));
		assertEquals(88, obj.get(2));
	}

	@Test
	public void Get_NonExistingPosition_WrongInput() {
		LinkedList obj = new LinkedList();
		obj.add(0, 77);
		obj.add(1, 3);
		obj.add(2, 88);
		assertEquals(Integer.MIN_VALUE, obj.get(63));
	}

	@Test
	public void Remove_AllExistingPositions_True() {
		LinkedList obj = new LinkedList();
		obj.add(0, 77);
		obj.add(1, 3);
		obj.add(2, 88);

		assertTrue(obj.remove(2));
		assertTrue(obj.remove(1));
		assertTrue(obj.remove(0));
	}

	@Test
	public void Remove_NonExistingPosition_False() {
		LinkedList obj = new LinkedList();
		obj.add(0, 77);
		obj.add(1, 3);
		obj.add(2, 88);

		assertTrue(obj.remove(2));
		assertTrue(obj.remove(1));
		assertTrue(obj.remove(0));
		assertFalse(obj.remove(0));
	}

	@Test
	public void IsEmpty_NotEmptyList_False() {
		LinkedList obj = new LinkedList();
		obj.add(0, 77);
		obj.add(1, 3);
		obj.add(2, 88);
		assertFalse(obj.isEmpty());
	}

	@Test
	public void IsEmpty_EmptyList_True() {
		LinkedList obj = new LinkedList();
		assertTrue(obj.isEmpty());
	}

}
