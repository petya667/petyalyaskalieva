package dataStructuresAndAlgorithmsInJava;

import static org.junit.Assert.*;

import org.junit.Test;

import dataStructuresAndAlgorithmsInJava.MinMissingForSortedArray;

public class MinMissingForSortedArrayTest {

	@Test
	public void MinMissing_Array_Min() {
		MinMissingForSortedArray obj = new MinMissingForSortedArray();
		int[] arr = { 1, 3, 4, 5 };
		assertEquals(2, obj.minMissing(arr));
	}

	@Test
	public void MinMissing_ArrayWithDuplicateElements_Min() {
		MinMissingForSortedArray obj = new MinMissingForSortedArray();
		int[] arr = { 1, 1, 2, 3, 3 };
		assertEquals(4, obj.minMissing(arr));
	}
}
