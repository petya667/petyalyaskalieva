package dataStructuresAndAlgorithmsInJava;

import static org.junit.Assert.*;

import org.junit.Test;

import dataStructuresAndAlgorithmsInJava.CompressText;

public class CompressTextTest {

	@Test
	public void Compress_Text_Compressed() {
		CompressText obj = new CompressText();
		assertEquals("a4t3o4", obj.compress("aaaatttoooo"));
	}

	@Test
	public void Compress_Text_NotCompressed() {
		CompressText obj = new CompressText();
		assertEquals("aattto", obj.compress("aattto"));
	}
	@Test
	public void Compress_EmptyString_EmptyString() {
		CompressText obj = new CompressText();
		assertEquals("", obj.compress(""));
	}
}
