package dataStructuresAndAlgorithmsInJava;

import static org.junit.Assert.*;

import org.junit.Test;

import dataStructuresAndAlgorithmsInJava.BracketsCorrectness;

public class BracketsCorrectnessTest {

	@Test
	public void Correct_OnlyBrackets_True() {
		BracketsCorrectness obj = new BracketsCorrectness();
		assertTrue(obj.correct("[]({[]})()"));
	}

	@Test
	public void Correct_OnlyBrackets_False() {
		BracketsCorrectness obj = new BracketsCorrectness();
		assertFalse(obj.correct("[]({[{{{]})()"));
	}

	@Test
	public void Correct_String_True() {
		BracketsCorrectness obj = new BracketsCorrectness();
		assertTrue(obj.correct("[464]({21[4651]})(545)"));
	}
}
