package dataStructuresAndAlgorithmsInJava;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import dataStructuresAndAlgorithmsInJava.TriangleRule;

public class TriangleRuleTest {

	@Test
	public void Find3Numbers_ArrayWith2Numbers_Null() {
        TriangleRule obj = new TriangleRule();
        int array[]= {7,8};
		assertNull(obj.find3Numbers(array));
	}
	@Test
	public void Find3Numbers_ArrayWith3Numbers_Null() {
        TriangleRule obj = new TriangleRule();
        int array[]= {4,1,2};
		assertNull(obj.find3Numbers(array));
	}
	@Test
	public void Find3Numbers_Array_Array() {
        TriangleRule obj = new TriangleRule();
        int array[]= {5, 4, 3, 1, 2};
        int[] result = {2,3,4};
		assertArrayEquals(result,obj.find3Numbers(array));
	}
}
