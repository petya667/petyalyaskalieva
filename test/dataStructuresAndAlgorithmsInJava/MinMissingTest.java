package dataStructuresAndAlgorithmsInJava;

import static org.junit.Assert.*;

import org.junit.Test;

import dataStructuresAndAlgorithmsInJava.MinMissing;

public class MinMissingTest {

	@Test
	public void MinMissing_ArrayWithout1_Min() {
		MinMissing obj = new MinMissing();
		int[] array = { 3, 2, 5, 10 };
		assertEquals(1, obj.minMissing(array));
	}

	@Test
	public void MinMissing_ArrayWith1_Min() {
		MinMissing obj = new MinMissing();
		int[] array = { 1, 6, 4, 2, 5, 3 };
		assertEquals(7, obj.minMissing(array));
	}

	@Test
	public void MinMissing_ArrayWithDuplicatingElements_Min() {
		MinMissing obj = new MinMissing();
		int[] array = { 1, 6, 4, 2, 5, 3, 3, 3 };
		assertEquals(7, obj.minMissing(array));
	}
}
