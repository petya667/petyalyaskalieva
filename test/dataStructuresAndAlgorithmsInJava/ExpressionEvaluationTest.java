package dataStructuresAndAlgorithmsInJava;

import static org.junit.Assert.*;

import org.junit.Test;

import dataStructuresAndAlgorithmsInJava.ExpressionEvaluation;

public class ExpressionEvaluationTest {

	@Test
	public void ExpressionEvaluation_String_Result() {
		ExpressionEvaluation obj = new ExpressionEvaluation();
		assertEquals(4, obj.expressionEvaluation("(((2 * 5) - (1 * 2)) / (9 - 7))"));
	}

}
