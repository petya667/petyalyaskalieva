package dataStructuresAndAlgorithmsInJava;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import dataStructuresAndAlgorithmsInJava.StockSpanProblem;

public class StockSpanProblemTest {

	@Test
	public void FindSpan_Array_StockSpan() {
		StockSpanProblem obj =new StockSpanProblem();
		int[] a = {100,80,60,70,60,75,85};
		int[] result= {1,1,1,2,1,4,6};
		assertArrayEquals(result, obj.findSpan(a, 7));
	}

}
