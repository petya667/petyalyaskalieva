package dataStructuresAndAlgorithmsInJava;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import dataStructuresAndAlgorithmsInJava.ClosestNumber;

public class ClosestNumberTest {

	@Test
	public void FindClosest_Array_ArrayWithClosestNumb() {
		ClosestNumber obj = new ClosestNumber();
		int[] input = { 3, 2, 5, 10, 4 };
		int result[] = { 5, 5, 10, -1, -1 };
		assertArrayEquals(result, obj.findClosest(input));
	}

}
