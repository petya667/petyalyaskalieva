package dataStructuresAndAlgorithmsInJava;

import static org.junit.Assert.*;

import org.junit.Test;

import dataStructuresAndAlgorithmsInJava.StringExpansion;

public class StringExpansionTest {

	@Test
	public void Expand_String_ExpandedString() {
      StringExpansion obj = new StringExpansion();
		assertEquals("ABDCDCDCFF",obj.expand("AB3(DC)2(F)"));
	}

}
