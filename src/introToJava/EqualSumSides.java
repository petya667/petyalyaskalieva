package introToJava;

public class EqualSumSides {
	boolean equalSumSides1(int[] numbers) {
		int sum1 = 0;
		int sum2 = 0;
		for (int j = 0; j < numbers.length - 1; j++)// which we can remove
		{
			sum1 = 0;
			sum2 = 0;
			for (int i = 0; i < j; i++) {
				sum1 += numbers[i];
			}
			for (int i = j + 1; i < numbers.length; i++) {
				sum2 += numbers[i];
			}
			if (sum1 == sum2)
				return true;
		}
		return false;
	}

	public static void main(String[] args) {
		int a[] = new int[5];
		a[0] = 3;
		a[1] = 0;
		a[2] = -1;
		a[3] = 2;
		a[4] = 1;
		EqualSumSides obj = new EqualSumSides();
		System.out.println(obj.equalSumSides1(a));
		int b[] = new int[6];
		b[0] = 2;
		b[1] = 1;
		b[2] = 2;
		b[3] = 3;
		b[4] = 1;
		b[5] = 4;
        int c[]= {8,8};
		System.out.println(obj.equalSumSides1(b));
		System.out.println(obj.equalSumSides1(c));
	}
}
