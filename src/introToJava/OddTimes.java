package introToJava;

public class OddTimes {
	int getOddOccurrence(int[] array) {
		int counter = 0;
		for (int i = 0; i < array.length - 1; i++) {
			counter = 1;
			if (array.length == 1) {
				return array[0];
			}
			for (int j = 0; j < array.length; j++) {
				if (array[j] == array[i] && i != j) {
					counter++;
				}
			}
			if (counter % 2 != 0) {
				return array[i];
			}
		}
		return -1;
	}

	public static void main(String[] args) {
		OddTimes obj = new OddTimes();
		int a[] = new int[7];
		a[0] = 1;
		a[1] = 2;
		a[2] = 7;
		a[3] = 5;
		a[4] = 1;
		a[5] = 2;
		a[6] = 1;

		System.out.println(obj.getOddOccurrence(a));
	}

}
