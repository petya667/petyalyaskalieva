package introToJava;

public class Multiple { // NOK

	static boolean isPrime(int n) {
		if (n <= 1) {
			return false;
		}
		for (int i = 2; i <= (n - 1); i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}

	long getSmallestMultiple(int n) {
		int a[] = new int[n];
		for (int i = 0; i < n; i++) {
			a[i] = i + 1;
		}
		int counter = 0;
		for (int i = 0; i < n; i++) // to find the number of prime numbers
		{
			if (isPrime(a[i]) == true)
				counter++;
		}
		int[] prime = new int[counter];
		int d = 0;
		for (int i = 0; i < counter; i++) // we fill it the array with prime numbers
		{
			while (d < n) {
				if (isPrime(a[d]) == true) {
					prime[i] = a[d];
					d++;
					break;
				}
				d++;
			}
		}
		long nok = 1;
		boolean check2 = true;
		for (int i = 0; i < counter; i++) // this may be the error
		{
			if (check2 == false) {
				i--;// we return to the same prime number
			}
			check2 = true;
			for (int j = 0; j < n; j++) {
				if (a[j] % prime[i] == 0) {
					a[j] /= prime[i];
					if (a[j] % prime[i] == 0) {
						check2 = false;
					}
				}
			}

			nok *= prime[i];

		}
		return nok;
	}

	public static void main(String[] args) {
		Multiple obj = new Multiple();
		System.out.println(obj.getSmallestMultiple(6));

	}

}
