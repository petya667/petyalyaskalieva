package introToJava;

public class Palindrome {
	boolean isPalindrome(String argument) {
		int i, len = argument.length();
		StringBuilder dest = new StringBuilder(len);

		for (i = (len - 1); i >= 0; i--) {
			dest.append(argument.charAt(i));
		}

		if (argument.contentEquals(dest))
			return true;
		return false;
	}

	public static void main(String[] args) {
		Palindrome obj = new Palindrome();
		System.out.println(obj.isPalindrome("abcdcba"));
	}

}
