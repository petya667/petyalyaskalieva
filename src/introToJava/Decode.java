package introToJava;

public class Decode {
	String decodeUrl(String input) {
		int i, len = input.length();
		StringBuilder dest = new StringBuilder(len);
		String[] codings = { "%20", "%3A", "%3D", "%2F" // 3 chars
		};
		boolean newAppend = true;
		for (i = 0; i <= len - 1; i++) {
			if (input.charAt(i) == (codings[0].charAt(0)) && i <= len - 3) { // %
				for (int j = 0; j < 4; j++) {
					if (input.charAt(i + 1) == (codings[j].charAt(1)) && (i <= len - 2)) {
						if (input.charAt(i + 2) == (codings[j].charAt(2))) {
							if (j == 0) {
								dest.append(' ');
								newAppend = false;
							} else if (j == 1) {
								dest.append(':');
								newAppend = false;
							} else if (j == 2) {
								dest.append('?');
								newAppend = false;
							} else {
								dest.append('/');
								newAppend = false;
							}
							break;
						}
					}
				}
				if (newAppend == false) {
					i += 2;
				} else {
					dest.append(input.charAt(i));
				}
			} else {
				dest.append(input.charAt(i));
			}
			newAppend = true;
		}
		return dest.toString();
	}

	public static void main(String[] args) {
		String input = "%20ngj%jd%3A";
		Decode obj = new Decode();
		System.out.println(obj.decodeUrl(input));
	}

}