package introToJava;

import java.util.Arrays;

public class MaxScalar {
	// scalar product depends on a, b , cos(a,b)
	long maximalScalarSum(int[] a, int[] b) {
		Arrays.sort(a);
		Arrays.sort(b);
		int product = 0;
		for (int i = 0; i < a.length; i++) {
			product += (a[i] + b[i]);
		}
		return product;
	}

	public static void main(String[] args) {
		MaxScalar obj = new MaxScalar();
		int a[] = new int[4];
		int b[] = new int[4];
		a[0] = 4;
		a[1] = 1;
		a[2] = 5;
		b[0] = 3;
		b[1] = 7;
		b[2] = 10;
		System.out.println(obj.maximalScalarSum(a, b));

	}

}
