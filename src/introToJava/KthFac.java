package introToJava;

public class KthFac {
	long fac(long n) {
		long p = 1;
		for (int i = 0; i < n; i++) {
			p *= (i + 1);
		}
		return p;
	}

	long kthFac(int k, int n) {
		if (k <= 0)
			return 1;

		long result = fac(n);
		for (int i = 2; i <= k; i++) {
			result = fac(result);
		}
		return result;
	}

	public static void main(String[] args) {
		KthFac obj = new KthFac();
		System.out.println(obj.kthFac(2, 3));

	}

}
