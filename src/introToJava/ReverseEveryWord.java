package introToJava;

public class ReverseEveryWord {
	String reverseEveryWord(String arg) {
		int i, len = arg.length();
		StringBuilder dest = new StringBuilder(len);
		String[] words = arg.split(" ");

		for (i = (words.length - 1); i >= 0; i--) {
			for (int j = (words[i].length() - 1); j >= 0; j--) {

				dest.append(words[i].charAt(j));
			}
			if(i!=0)
			dest.append(" ");
		}

		return dest.toString();
	}

	public static void main(String[] args) {
		ReverseEveryWord obj = new ReverseEveryWord();
		String input = "What is this";
		System.out.println(obj.reverseEveryWord(input));

	}

}