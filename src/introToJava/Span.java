package introToJava;
//import java.util.Arrays;

public class Span {
	int maxSpan(int[] numbers) {
		if (numbers.length == 1) {
			return 0;
		}
		int max = 0;
		int span = 0;
		for (int i = 0; i < numbers.length; i++) {
			for (int j = i+1; j < numbers.length; j++) {
				if (numbers[i] == numbers[j]) // counts the number of equal numbers
				{
					span = j - i + 1;
					if (span > max) {
						max = span;
					}
				}
			}
		}
		return max;
	}

	public static void main(String[] args) {
		Span obj = new Span();

		int b[] = new int[6];
		b[0] = 2;
		b[1] = 5;
		b[2] = 4;
		b[3] = 1;
		b[4] = 3;
		b[5] = 4;
		System.out.println(obj.maxSpan(b));
	}
}
