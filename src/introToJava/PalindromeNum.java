package introToJava;

public class PalindromeNum {
	boolean isPalindrome(int argument) {
		int num = argument;
		int n = 0;
		while (num != 0) {
			num /= 10;
			n++;
		}
		num = argument;
		int[] arg = new int[n];
		int[] arg2 = new int[n];
		n--;
		for (int i = 0; i < arg.length; i++) {
			arg[i] = num % 10;
			num /= 10;
			arg2[n] = arg[i];
			n--;
		}
		for (int i = 0; i < arg2.length; i++) {
			if (arg[i] != arg2[i])
				return false;
		}

		return true;
	}

	public static void main(String[] args) {
		PalindromeNum obj = new PalindromeNum();
		System.out.println(obj.isPalindrome(1234321));
	}

}
