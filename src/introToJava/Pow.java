package introToJava;

public class Pow {

	long pow(int x, int y) {

		long temp;
		if (y == 0)
			return 1;
		temp = pow(x, y / 2);
		if (y % 2 == 0)
			return temp * temp;
		else {
			if (y > 0)
				return x * temp * temp;
			else
				return (temp * temp) / x;
		}
	}

	public static void main(String[] args) {

	}

}
