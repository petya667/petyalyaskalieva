package introToJava;

public class KthMin {
	int kthMin(int k, int[] array) {
		if (k > array.length) {
			System.out.println("wrong input");
			return -1;
		}
		int rightmost = array.length - 1;
		for (int i = 0; i < rightmost; i++) {
			for (int j = rightmost; j > i; j--) {
				if (array[j - 1] > array[j]) {
					int swap = array[j - 1];
					array[j - 1] = array[j];
					array[j] = swap;
				}
			}
		}
		return array[k - 1];
	}

	public static void main(String[] args) {
		KthMin obj = new KthMin();
		int[] a = new int[3];
		a[0] = 1;
		a[1] = -2;
		a[2] = 30;
		System.out.println(obj.kthMin(2, a));
	}

}
