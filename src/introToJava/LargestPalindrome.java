package introToJava;

public class LargestPalindrome {
	boolean isPalindrome(long argument) {
		long num = argument;
		int n = 0;
		while (num != 0) {
			num /= 10;
			n++;
		}
		num = argument;
		long[] arg = new long[n];
		long[] arg2 = new long[n];
		n--;
		for (int i = 0; i < arg.length; i++) {
			arg[i] = num % 10;
			num /= 10;
			arg2[n] = arg[i];
			n--;
		}
		for (int i = 0; i < arg2.length; i++) {
			if (arg[i] != arg2[i])
				return false;
		}

		return true;
	}

	long getLargestPalindrome(long N) {
		LargestPalindrome obj = new LargestPalindrome();
		for (long i = N; i > 0; i--) {
			if (obj.isPalindrome(i) == true)
				return i;
		}
		return 0;
	}

	public static void main(String[] args) {

		LargestPalindrome obj = new LargestPalindrome();
		System.out.println(obj.getLargestPalindrome(122));
	}

}
