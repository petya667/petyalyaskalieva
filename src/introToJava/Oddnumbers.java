package introToJava;

public class Oddnumbers {

	boolean isOdd(int n) {
		if (n % 2 == 0) {
			return false;
		} else {
			return true;
		}

	}

	public static void main(String[] args) {
		Oddnumbers obj = new Oddnumbers();
		System.out.println(obj.isOdd(5));
		System.out.println(obj.isOdd(6));
	}
}
