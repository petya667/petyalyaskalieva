package introToJava;

public class MinElement {
	int min(int[] array) {
		int min = array[0];
		for (int i = 1; i < array.length; i++) {
			if (array[i] < min) {
				min = array[i];
			}
		}
		return min;
	}

	public static void main(String[] args) {
		int[] a = new int[3];
		a[0] = 1;
		a[1] = -2;
		a[2] = 30;

		MinElement obj = new MinElement();

		System.out.println(obj.min(a));
	}

}
