package introToJava;

public class Reverse {
	String reverse(String argument) {

		int i, len = argument.length();
		StringBuilder dest = new StringBuilder(len);

		for (i = (len - 1); i >= 0; i--) {
			dest.append(argument.charAt(i));
		}

		return dest.toString();
	}

	public static void main(String[] args) {
		Reverse obj = new Reverse();
		System.out.println(obj.reverse("hello"));
	}

}
