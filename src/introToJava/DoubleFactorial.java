package introToJava;

public class DoubleFactorial {
	long fac(long n) {
		long p = 1;
		for (int i = 0; i < n; i++) {
			p *= (i + 1);
		}
		return p;
	}

	long doubleFac(long n) {
		long result = fac(n);
		return fac(result);
	}

	public static void main(String[] args) {
		DoubleFactorial obj = new DoubleFactorial();
		System.out.println(obj.doubleFac(3L));

	}

}
