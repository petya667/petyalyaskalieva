package introToJava;

public class Average {
	int getAverage(int[] array) {
		int sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		return sum / array.length;
	}

	public static void main(String[] args) {
		Average obj = new Average();
		int a[] = new int[4];
		a[0] = 2;
		a[1] = 2;
		a[2] = 3;
		a[3] = 3;
		System.out.println(obj.getAverage(a));
	}

}
