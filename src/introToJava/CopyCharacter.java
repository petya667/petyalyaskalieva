package introToJava;

public class CopyCharacter {
	String copyChars(String input, int k) {
		int i, len = input.length();
		StringBuilder dest = new StringBuilder(len * 3);

		for (int j = 0; j < k; j++) {
			for (i = 0; i < len; i++) {
				dest.append(input.charAt(i));
			}
		}

		return dest.toString();
	}

	public static void main(String[] args) {
		CopyCharacter obj = new CopyCharacter();
		System.out.println(obj.copyChars("nbsp;", 3));
	}

}
