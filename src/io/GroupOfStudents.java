package io;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import io.Student.Gender;

public class GroupOfStudents {
	private List<Student> students;

	public GroupOfStudents() {
		super();
		students = new ArrayList<>();
	}

	public void addStudent(Student st) {
		students.add(st);
	}

	public void addListOfStudents(List<Student> list) {
		students.addAll(list);
	}

	public double getAverageMark() {
		if (students.isEmpty()) {
			return 0.0;
		}
		double sum = 0.0;
		sum = students.stream().map(Student::getMark).reduce(0.0, (a, b) -> a + b);
		return sum / students.size();
	}

	public List<Student> getAllPassingStudents() {
		return students.stream().filter(student -> student.getMark() >= 3.0).collect(Collectors.toList());
	}

	public List<Student> getAllFailingStudents() {
		return students.stream().filter(student -> student.getMark() < 3.0).collect(Collectors.toList());
	}

	public Map<Boolean, List<Student>> splitbyMarks(double markSeparator) {
		return students.stream().collect(Collectors.partitioningBy(student -> student.getMark() >= markSeparator));
	}

	public List<Student> orderByMarksAscending() {
		return students.stream().sorted(Comparator.comparingDouble(Student::getMark)).collect(Collectors.toList());
	}

	public List<Student> orderByMarksDescending() {
		return students.stream().sorted(Comparator.comparingDouble(Student::getMark).reversed())
				.collect(Collectors.toList());
	}

	public List<Student> clusterizeAndReturnStudentsWithHighestMarks() {
		double mark = students.stream().map(Student::getMark).reduce(2.0, Double::max);
		return students.stream().filter(student -> student.getMark() == mark).collect(Collectors.toList());
	}

	public List<Student> clusterizeAndReturnStudentsWithLowestMarks() {
		double mark = students.stream().map(Student::getMark).reduce(2.0, Double::min);
		return students.stream().filter(student -> student.getMark() == mark).collect(Collectors.toList());
	}

	public Map<Integer, List<Double>> getMarksDistributionByAge() {
		return students.stream().collect(
				Collectors.groupingBy(Student::getAge, Collectors.mapping(Student::getMark, Collectors.toList())));
	}

	public double getAverageMarkByGender(Gender gender) {
		double sum = students.stream().filter(student -> student.getGender().equals(gender)).map(Student::getMark)
				.reduce(0.0, (a, b) -> a + b);
		return sum / students.stream().filter(student -> student.getGender().equals(gender)).count();
	}

	public Map<Double, List<Student>> getMarksByDistribution() {
		return students.stream().collect(Collectors.groupingBy(Student::getMark, Collectors.toList()));
	}

	public List<String> getEmail() {
		return students.stream().map(Student::getEmail).collect(Collectors.toList());
	}

	public Map<Gender, Map<Integer, List<Student>>> splitByGenderAndPartitionByAge() {
		return students.stream().collect(
				Collectors.groupingBy(Student::getGender, Collectors.groupingBy(Student::getAge, Collectors.toList())));
	}
}
