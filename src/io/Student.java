package io;

public class Student {
	private String name;
	private double mark;
	private String email;

	public enum Gender {
		FEMALE, MALE
	}

	Gender gender;
	private int age;

	public Student(String name, double mark, String email, Gender gender, int age) {
		super();
		this.name = name;
		this.mark = mark;
		this.email = email;
		this.gender = gender;
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getMark() {
		return mark;
	}

	public void setMark(double mark) {
		this.mark = mark;
	}

	public static void main(String[] args) {

	}

}
