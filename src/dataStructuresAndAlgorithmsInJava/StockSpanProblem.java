package dataStructuresAndAlgorithmsInJava;

import java.util.Stack;

public class StockSpanProblem {
//we use a stack as an abstract data type to store the days i, h(i), h(h(i)) and so on
	// array P which contains the daily prices of the stocks
	public int[] findSpan(int[] P, int n) {
		Stack<Integer> stack = new Stack<>();
		// array S which will store the span of the stock
		int[] S = new int[n];
		stack.push(0); // index of first element
		S[0] = 1;
		for (int i = 1; i < n; i++) {

			while (!stack.isEmpty() && P[i] >= P[stack.peek()]) {
				stack.pop();
			}
			if (stack.isEmpty()) { // P[i] is greater than all elements on left of it
				S[i] = i + 1;
			} else {
				S[i] = i - stack.peek();
			}
			stack.push(i);

		}
		return S;
	}
}
