package dataStructuresAndAlgorithmsInJava;

public class MinMissing {
	public int minMissing(int[] array) { // without sorting
		for (int i = 1; i < array.length + 1; i++) {
			for (int j = 0; j < array.length; j++) {
				if (array[j] == i) {
					break;
				} else if (j == array.length - 1) {
					return i;
				}
			}
			if (i == array.length) {
				return i+1;
			}
		}
		return 0;
	}


}
