package dataStructuresAndAlgorithmsInJava;

public class MinMissingForSortedArray {
	public int minMissing(int[] array) {
		for (int i = 1; i < array.length + 1; i++) {
			if (i == array.length) {
				return array[i - 1] + 1;
			}

			else if (array[i - 1] != i && array[i - 1] != i - 1) {
				return i;
			}
		}
		return 0;
	}
}
