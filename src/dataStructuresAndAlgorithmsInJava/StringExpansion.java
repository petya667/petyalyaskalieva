package dataStructuresAndAlgorithmsInJava;

public class StringExpansion { // AB3(DC)2(F) - expand to ABDCDCDCFF
	public String expand(String input) {
		StringBuilder expanded = new StringBuilder();
		String[] array = input.split("\\(");
		char ch;
		int op1 = 0;
		int j = 0;
		for (int i = 0; i < array.length; i++) {
			if (array.length == 1) {
				return input;
			}
			if (i == 0) {
				j = 0;
				ch = array[0].charAt(j);
				while (!Character.isDigit(ch)) {
					expanded.append(ch);
					j++;
					ch = array[0].charAt(j);
				}
				op1 = getNumber(array, 0, j);
				continue;
			} else if (i > 0 && i != array.length - 1) { // middle
				j = 0;
				ch = array[i].charAt(j);
				StringBuffer tmp = new StringBuffer();
				while (ch != ')') {
					tmp.append(ch);
					j++;
					ch = array[i].charAt(j);
				}
				while (op1 > 0) {
					expanded.append(tmp);
					op1--;
				}
				j++;
				ch = array[i].charAt(j);
				while (!Character.isDigit(ch)) {
					expanded.append(ch);
					j++;
					ch = array[i].charAt(j);
				}
				op1 = getNumber(array, i, j);
				continue;
			} else if (i == array.length - 1) {
				j = 0;
				ch = array[i].charAt(j);
				StringBuffer tmp = new StringBuffer();
				while (ch != ')') {
					tmp.append(ch);
					ch = array[i].charAt(++j);
				}
				while (op1 > 0) {
					expanded.append(tmp);
					op1--;
				}
				j++;
				if (j < array[i].length() - 1)
					ch = array[i].charAt(j);
				while (j < array[i].length()) {
					expanded.append(ch);
					ch = array[i].charAt(++j);
				}
			}
		}
		return expanded.toString();
	}

	public int getNumber(String[] array, int indexArr, int indexChar) {
		int op1 = 0;
		while (array[indexArr].length() > indexChar) {
			int tmp = Character.getNumericValue(array[indexArr].charAt(indexChar));
			op1 = (op1 * 10) + tmp;
			indexChar++;
		}
		return op1;
	}

	public static void main(String[] args) {
		StringExpansion obj = new StringExpansion();
		System.out.println(obj.expand("AB3(DC)2(F)"));
	}
}
