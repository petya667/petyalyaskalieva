package dataStructuresAndAlgorithmsInJava;

public class CompressText {
	public String compress(String input) {
		String out = "";
		if (input == "") {
			return "";
		}
		int sum = 1;
		for (int i = 0; i < input.length() - 1; i++) {
			if (input.charAt(i) == input.charAt(i + 1)) {
				sum++;
			} else {
				out = out + input.charAt(i) + sum;
				sum = 1;
			}
		}
		out = out + input.charAt(input.length() - 1) + sum;
		return out.length() < input.length() ? out : input;
	}

}
