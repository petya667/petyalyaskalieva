package dataStructuresAndAlgorithmsInJava;

public class Listnode {
		    private Object data;
		    private Listnode next;

		    public Listnode(Object d) {
			this(d, null);
		    }

		    public Listnode(Object d, Listnode n) {
			data = d;
			next = n;
		    }
		    
		    public Object getData() {
		        return data;
		    }

		    public Listnode getNext() {
		        return next;
		    }

		    public void setData(Object ob) {
		        data = ob;
		    }

		    public void setNext(Listnode n) {
		        next = n;
		    }
		

}
