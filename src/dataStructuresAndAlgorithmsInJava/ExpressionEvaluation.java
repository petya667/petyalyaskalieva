package dataStructuresAndAlgorithmsInJava;

import java.util.Stack;

public class ExpressionEvaluation {
	public int expressionEvaluation(String input) // Evaluation of an Infix Expression that is Fully Parenthesized
	{
		int len = input.length();
		Stack<Character> chars = new Stack<>();
		Stack<Integer> values = new Stack<>();
		char ch, op;
		int op1, op2;
		for (int i = 0; i < len; i++) {
			ch = input.charAt(i);
			if (ch == ' ')
				continue;
			switch (ch) {

			case '+':
			case '-':
			case '/':
			case '*':
			case '(':
				chars.push(ch);
				break;
			case ')':
				op2 = values.pop();
				op1 = values.pop();
				op = chars.pop();
				while (op != '(') {
					values.push(eval(op, op1, op2));
					op = chars.pop();
					if (op != '(') {
						op2 = values.pop();
						op1 = values.pop();
					}
				}
				if (i + 1 == len) {
					while (!chars.isEmpty()) {
						op = chars.pop();
						op2 = values.pop();
						op1 = values.pop();
						values.push(eval(op, op1, op2));
					}
					op1 = values.pop();
					System.out.println("The final solution is: " + op1);
					return op1;
				}
				break;
			default:
				values.push(ch - '0');
			}

		}
		return 0;
	}

	private int eval(char op, int op1, int op2) {
		switch (op) {
		case '+':
			return (op1 + op2);
		case '-':
			return (op1 - op2);
		case '/':
			return (op1 / op2);
		case '*':
			return (op1 * op2);
		}
		return 0;
	}


}
