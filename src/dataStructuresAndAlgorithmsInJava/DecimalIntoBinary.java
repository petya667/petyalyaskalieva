package dataStructuresAndAlgorithmsInJava;

import java.util.Stack;

public class DecimalIntoBinary {
	public void decimalIntoBinary(int n) {
		Stack<Integer> binary = new Stack<>();
		int digit;
		while (n > 0) {
			digit = n % 2;
			if (binary.capacity() != binary.size()) {
				binary.push(digit);
			} else {
				System.out.println("Stack is full!");
				return;
			}
			n /= 2;
		}
		while (binary.isEmpty() == false) {
			System.out.print(binary.pop());
		}
	}

	public static void main(String[] args) {
		DecimalIntoBinary obj = new DecimalIntoBinary();
		obj.decimalIntoBinary(18);
	}

}
