package dataStructuresAndAlgorithmsInJava;

//import tasks0.Node;

public class LinkedList {
	Node first;
	int size;

	public LinkedList() {
		first = null;
		size = 0;
	}

	boolean add(int position, int data) {
		if (position < 0 || position > size) {
			return false; //
		}
		if (position == 0) {
			Node toAdd = new Node(data, first);
			first = toAdd;
			size++;
		} else {
			Node current = getNode(position - 1);
			Node toAdd = new Node(data, current.getNext());
			current.setNext(toAdd);
			size++;
		}
		return true;
	}

	int get(int position) {
		if (position < 0 || position > size - 1) {
			return Integer.MIN_VALUE;
		}
		return getNode(position).getData();
	}

	boolean remove(int position) {
		if (position < 0 || position > size - 1) {
			return false;
		}
		if(position == 0)
		{
			first = first.getNext();
			size--;
			return true;
		}

		Node current = getNode(position - 1);
		Node toDelete = current.getNext();
		current.setNext(toDelete.getNext());
		size--;
		return true;
	}

	private Node getNode(int position) {
		Node current = first;
		for (int i = 0; i < position; i++) {
			current = current.getNext();
		}
		return current;
	}

	boolean isEmpty() {
		return first == null;
	}

	public int getSize() {
		return size;
	}

	public static void main(String[] args) {

	}

}
