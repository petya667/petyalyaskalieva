package dataStructuresAndAlgorithmsInJava;

//import tasks.Listnode;

public class LinkedListStackImplementation {

	private Listnode items; // pointer to the linked list of items in the stack

	private int numItems; 

	public LinkedListStackImplementation() {
       
		numItems = 0;
	}

	public void push(Object ob) {
		items = new Listnode(ob, items);
		numItems++;
	}

	public Object pop() {
		if (numItems == 0) {
			return null;
		}
		Object tmp = items.getData(); // Remove the first node from the list by setting items = items.next
		// first node has already been removed from the list, so we need to save its
		// value in order to return it
		items = items.getNext(); // the next in stack = the second element of the list
		numItems--;
		return tmp;
	}

	public Object peek() {
		if (numItems == 0) {
			return null;
		}
		return items.getData();
	}

	public int size() {
		return numItems;
	}

	public boolean empty() {
		if (numItems == 0) {
			return true;
		}
		return false;
	}

	public static void main(String[] args) {

	}

}
