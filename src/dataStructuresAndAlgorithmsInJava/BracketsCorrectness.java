package dataStructuresAndAlgorithmsInJava;

import java.util.Stack;

public class BracketsCorrectness {
	public boolean correct(String input) {
		int len = input.length();
		char ch;
		Stack<Character> chars = new Stack<Character>();
		for (int i = 0; i < len; i++) {
			ch = input.charAt(i);
			switch (ch) {
			case '(':
			case '{':
			case '[':
				chars.push(ch);
				break;
			case ')':
				if (chars.peek() == '(') {
					chars.pop();
				} else {
					return false;
				}
				break;
			case '}':
				if (chars.peek() == '{') {
					chars.pop();
				} else {
					return false;
				}
				break;
			case ']':
				if (chars.peek() == '[') {
					chars.pop();
				} else {
					return false;
				}
				break;
			default:
				continue;
			}
		}
		return true;
	}
}
