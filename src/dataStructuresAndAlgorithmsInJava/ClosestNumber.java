package dataStructuresAndAlgorithmsInJava;

public class ClosestNumber {
	public int[] findClosest(int[] array) {
		int[] result = new int[array.length];
		for (int i = 0; i < array.length; i++) {
			int close = -1;
			for (int j = i + 1; j < array.length; j++) {
				if (array[i] < array[j]) {
					close = array[j];
					break;
				}
			}
			result[i] = close;

		}
		return result;
	}
}
