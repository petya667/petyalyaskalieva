package dataStructuresAndAlgorithmsInJava;

public class TowersOfHanoi {
	void towersofHanoi(int n, int a, int b, int c) {
		if (n > 0) {
			towersofHanoi(n - 1, a, c, b); 
			System.out.println(" Move top disk from tower " + a + " to tower " + b);
			towersofHanoi(n - 1, c, b, a); 
		}
	}

	public static void main(String[] args) {
		TowersOfHanoi obj = new TowersOfHanoi();
		obj.towersofHanoi(3, 1, 2, 3);
	}

}
