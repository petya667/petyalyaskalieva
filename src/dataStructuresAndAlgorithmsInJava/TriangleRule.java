package dataStructuresAndAlgorithmsInJava;

import java.util.Arrays;

public class TriangleRule {
	public int[] find3Numbers(int[] array) {
		int len = array.length;
		if (len < 3) {
			return null;
		}
		int[] result = new int[3];
		Arrays.sort(array);
		for (int i = 0; i < array.length - 2; i++) {
			if (array[i] + array[i + 1] > array[i + 2]) {
            result[0]=array[i];
            result[1]=array[i+1];
            result[2]=array[i+2];
            return result;
			}
		}
		return null;
	}
}
