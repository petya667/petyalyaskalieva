package dataStructuresAndAlgorithmsInJava;

public class ArrayQueueImplementation {
	public class Queue {
		private static final int INITSIZE = 10; 
		private Object[] items; 
		private int numItems; 
		private int frontIndex;
		private int rearIndex;

		private void expandArray() {
			Object[] itemsNew = new Object[numItems * 2];
			System.arraycopy(items, frontIndex, itemsNew, frontIndex, items.length - frontIndex);
			if (frontIndex != 0) {
				System.arraycopy(items, 0, itemsNew, items.length, frontIndex);
			}
			items = itemsNew;
			rearIndex = frontIndex + numItems - 1;
		}

		public Queue() {
			items = new Object[INITSIZE];
			numItems = 0;
		}

		public void enqueue(Object ob) {
			if (numItems == items.length) {
				expandArray();
			}
			rearIndex = incrementIndex(rearIndex);

			items[rearIndex] = ob;
			numItems++;
		}

		private int incrementIndex(int index) {
			if (index == items.length - 1)
				return 0;
			else
				return index + 1;
		}

		public Object dequeue() {
			if (empty()) {
				System.out.println("Empty queue");
				return null;
			}
			frontIndex = incrementIndex(frontIndex);
			numItems--;
			return items[frontIndex];
		}

		public int size() {
			return numItems;
		}

		public boolean empty() {
			return numItems == 0;
		}
	}

	public static void main(String[] args) {

	}

}
