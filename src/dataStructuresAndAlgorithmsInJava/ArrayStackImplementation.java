                                                                                                                               package dataStructuresAndAlgorithmsInJava;

public class ArrayStackImplementation {
	private static final int INITSIZE = 10; 
	private Object[] items; 
	private int numItems; 

	private void expandArray() {
		Object[] itemsNew = new Object[numItems*2];
		System.arraycopy(items, 0, itemsNew, 0, items.length - 1);
		items = itemsNew;
	}

	public ArrayStackImplementation() {
		items = new Object[INITSIZE];
		numItems = 0;
	}

	public void push(Object ob) {
		if (numItems == INITSIZE) {
			expandArray(); 
		}
		items[numItems] = ob;
		numItems++;
	}

	public Object pop() {
		if (numItems == 0) {
			System.out.println("Empty array");
			return null;
		}
		numItems--;
		return items[numItems];
	}

	public Object peek() {
		if (numItems == 0) {
			System.out.println("Empty array");
			return null;
		}
		return items[numItems - 1];
	}

	public int size() {
		return numItems;
	}

	public boolean empty() {
		return numItems==0;
	}

	public static void main(String[] args) {

	}

}
