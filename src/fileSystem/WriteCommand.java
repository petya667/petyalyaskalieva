package fileSystem;

import java.nio.file.AccessDeniedException;
import java.nio.file.FileSystemException;

//check if the line is positive
public class WriteCommand implements Command {
	Terminal terminal;

	WriteCommand(Terminal terminal) {
		this.terminal = terminal;
	}

	@Override
	public String execute(String[] input) throws FileSystemException {
		if (input.length >= 4) {
			Directory dir = terminal.getFs().validateAndReturnTheParent(input[1], terminal.getCurrent());
			String fileName = terminal.getFs().getNameOfLastElementOfThePath(input[1]);
			if (!terminal.getFs().isFileAccesible(dir, fileName)) {
				throw new AccessDeniedException(fileName);
			}
			boolean overwrite = false;
			if (input[input.length - 1].equals("-overwrite")) {
				overwrite = true;
			}
			int i = 3;
			StringBuilder join = new StringBuilder();
			while (((i < input.length && overwrite == false) || (i < input.length - 1 && overwrite == true))) {
				join.append(input[i]);
				join.append(' ');
				i++;
			}
			if (join.length() != 0) {
				join.deleteCharAt(join.length() - 1); // deletes the last space
			}
			if (!(terminal.getFs().getTotalSize() + dir.getEventualIncreasingSizeOfFileAfterWriting(fileName,
					Integer.parseInt(input[2]), join.toString(), overwrite) <= terminal.getFs().getMEMORY())) {
				terminal.getFs().deleteNonAccessibleFiles();
			}
			if (terminal.getFs().isFileAccesible(dir, fileName)) {
				return dir.write(terminal.getFs().getNameOfLastElementOfThePath(input[1]), Integer.parseInt(input[2]),
						join.toString(), overwrite);
			} else {
				throw new AccessDeniedException(fileName);
			}
		} else {
			throw new FileSystemException(String.join(" ", input) + " missing file operand ");
		}
	}

}
