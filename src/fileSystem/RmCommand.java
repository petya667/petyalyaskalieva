package fileSystem;

import java.nio.file.FileSystemException;

public class RmCommand implements Command {
	Terminal terminal;

	RmCommand(Terminal terminal) {
		this.terminal = terminal;
	}

	@Override
	public String execute(String[] input) throws FileSystemException {
		if (input.length > 2) {
			for (int i = 1; i < input.length; i++) {
				Directory dir = terminal.getFs().validateAndReturnTheParent(input[i], terminal.getCurrent());
				terminal.getFs().addNonAccessibleFile(dir, terminal.getFs().getNameOfLastElementOfThePath(input[i]));
			}
		} else {
			throw new FileSystemException(input[0] + " missing file operand ");
		}
		return null;
	}

}
