package fileSystem;

import java.nio.file.FileSystemException;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PipeCommand implements Command {

	Terminal terminal;

	PipeCommand(Terminal terminal) {
		this.terminal = terminal;

	}

	@Override
	public String execute(String[] words) throws FileSystemException {
		List<Integer> pipeIndexes = new ArrayList<>();
		for (int i = 0; i < words.length; i++) {
			if (words[i].equals("|")) {
				pipeIndexes.add(i);
			}
		}
		String[] wordsBeforeTheFirstPipe = new String[pipeIndexes.get(0)]; // arguments passed to the
		// command after the pipe
		System.arraycopy(words, 0, wordsBeforeTheFirstPipe, 0, pipeIndexes.get(0));
		Map<String, Command> map = terminal.getCommands();
		String resultFromTheCommandBefore = map.get(wordsBeforeTheFirstPipe[0]).execute(wordsBeforeTheFirstPipe);
		String result = map.get(words[pipeIndexes.get(0) + 1]).execute(
				getArgumentsForTheCommandAfterAPipeWithSpaces(pipeIndexes, map, 0, resultFromTheCommandBefore, words));
		if (pipeIndexes.size() == 1) { // if there is only one pipe
			return result;
		}
		for (int i = 1; i < pipeIndexes.size(); i++) { // after the first pipe
			result = map.get(words[pipeIndexes.get(i) + 1])
					.execute(getArgumentsForTheCommandAfterAPipeWithSpaces(pipeIndexes, map, i, result, words));
		}
		return result;
	}

	private String[] getArgumentsForTheCommandAfterAPipeWithSpaces(List<Integer> pipeIndexes, Map<String, Command> map,
			int index, String resultFromTheCommandBefore, String[] words) throws NoSuchFileException {
		if (resultFromTheCommandBefore.equals("")) {
			throw new NoSuchFileException("");
		}
		List<String> listOfArgsForTheCommandAfterAPipe = new LinkedList<>();
		listOfArgsForTheCommandAfterAPipe.add(words[pipeIndexes.get(index) + 1]);
		if (words.length > pipeIndexes.get(index) + 2 && words[pipeIndexes.get(index) + 2].startsWith("-")) {
			listOfArgsForTheCommandAfterAPipe.add(words[pipeIndexes.get(index) + 2]);
		}
		if (!terminal.getFs().fileExists(resultFromTheCommandBefore, terminal.getCurrent())) {
			String[] array = resultFromTheCommandBefore.split(" ");
			for (int i = 0; i < array.length; i++) {
				listOfArgsForTheCommandAfterAPipe.add(array[i]);
			}
		} else {
			listOfArgsForTheCommandAfterAPipe.add(resultFromTheCommandBefore);
		}
		return listOfArgsForTheCommandAfterAPipe.toArray(new String[0]);
	}
}
