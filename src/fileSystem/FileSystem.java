package fileSystem;

import java.nio.file.FileAlreadyExistsException;
import java.nio.file.NoSuchFileException;
import java.util.HashMap;
import java.util.Map;

public class FileSystem {
	private Directory root;
	private final int MEMORY = 20;
	private int totalSize;
	private Map<Directory, String> nonAccessibleFiles; // key- parent directory, value - the name of the file
   // private Map<String,File> files;
	
	public FileSystem() throws FileAlreadyExistsException {
		root = new Directory("/");
		root.createDirectory("home");
		nonAccessibleFiles = new HashMap<>();
	}

	public FileSystem(Directory root) throws FileAlreadyExistsException {
		this.root = root;
		this.root.createDirectory("home");
		nonAccessibleFiles = new HashMap<>();
	}

	public void addNonAccessibleFile(Directory parent, String fileName) {
		nonAccessibleFiles.put(parent, fileName);
	}

	public void deleteNonAccessibleFiles() throws NoSuchFileException {
		for (HashMap.Entry<Directory, String> entry : nonAccessibleFiles.entrySet()) {
			entry.getKey().deleteFile(entry.getValue());
		}
	}

	public String getNameOfLastElementOfThePath(String input) {
		String[] splitted = input.split("/");
		return splitted[splitted.length - 1];
	}

	public boolean isFileAccesible(Directory dir, String name) {
		if (nonAccessibleFiles.containsKey(dir) && dir.contains(name)) {
			return false;
		}
		return true;
	}

	public Directory validateAndReturnTheParent(String path, Directory current) throws NoSuchFileException {
		// returns the *parent* current directory for the cd command and
//for the other operations the *parent of the* directory in which the actions are made
		if (path.startsWith("/")) {
			String[] splitted = path.substring(1).split("/"); // without the first one
			return getValidatedParent(splitted, getRoot());
		} else {
			String[] splitted = path.split("/"); // with the first one
			return getValidatedParent(splitted, current);
		}
	}

	public final int getMEMORY() {
		return MEMORY;
	}

	public int getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
	}

	private Directory getValidatedParent(String[] splitted, Directory dir) throws NoSuchFileException {

		for (int i = 0; i < splitted.length; i++) {
			if (i == splitted.length - 1) {
				return dir;
			}
			if (dir.contains(splitted[i]) || splitted[i].equals(".") || splitted[i].equals("..")) {
				dir = dir.getOneStepCloseDirectory(splitted[i]);
			} else {
				throw new NoSuchFileException(splitted[i]);
			}
		}
		return null;
	}

	public boolean fileExists(String input, Directory curr) {
		try {
			if (input.contains(" ")) {
				validateAndReturnTheParent(input, curr).countWords(getNameOfLastElementOfThePath(input.split(" ")[0]),
						false);
			} else {
				validateAndReturnTheParent(input, curr).countWords(getNameOfLastElementOfThePath(input), false);
			}
		} catch (NoSuchFileException e) { // if exception is thrown then accept the passed data as text
			return true;
		}
		return false;
	}

	public Directory getRoot() {
		return root;
	}

}
