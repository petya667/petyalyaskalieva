package fileSystem;

import java.nio.file.FileSystemException;

public class CdCommand implements Command {

	Terminal terminal;

	CdCommand(Terminal terminal) {
		this.terminal = terminal;
	}

	@Override
	public String execute(String[] input) throws FileSystemException {
		if (input.length != 2) {
			throw new FileSystemException(input[0] + " command not found");
		}
		Directory parentDirFromPath = terminal.getFs().validateAndReturnTheParent(input[1], terminal.getCurrent());
		terminal.setCurrent(
				parentDirFromPath.getOneStepCloseDirectory(terminal.getFs().getNameOfLastElementOfThePath(input[1])));
		return "";
	}

}
