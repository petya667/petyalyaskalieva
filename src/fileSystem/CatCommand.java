package fileSystem;

import java.nio.file.AccessDeniedException;
import java.nio.file.FileSystemException;

public class CatCommand implements Command {
	Terminal terminal;

	CatCommand(Terminal terminal) {
		this.terminal = terminal;
	}

	@Override
	public String execute(String[] input) throws FileSystemException {
		if (input.length > 1) {
			String[] result = new String[input.length - 1];
			for (int i = 1; i < input.length; i++) {
				Directory dir = terminal.getFs().validateAndReturnTheParent(input[i], terminal.getCurrent());
				String fileName = terminal.getFs().getNameOfLastElementOfThePath(input[i]);
				if (terminal.getFs().isFileAccesible(dir, fileName)) {
					String str = new String(dir.cat(fileName));
					System.out.println(str);
					result[i - 1] = str;
				} else {
					throw new AccessDeniedException(fileName);
				}
			}
			return String.join("\n", result);
		} else {
			throw new FileSystemException(input[0] + " missing file operand ");
		}
	}
}
