package fileSystem;

import java.nio.file.AccessDeniedException;
import java.nio.file.FileSystemException;
import java.nio.file.NoSuchFileException;

public class WordCountCommand implements Command {

	Terminal terminal;

	public WordCountCommand(Terminal terminal) {
		this.terminal = terminal;
	}

	@Override
	public String execute(String[] input) throws FileSystemException {
		boolean lines = false;
		Directory dir = null;
		int i = 1;
		if (input.length >= 2 && input[1].equals("-l")) {
			lines = true;
			i = 2;
		}
		boolean text = false;
		text = terminal.getFs().fileExists(input[i], terminal.getCurrent());
		if (!text) {
			StringBuilder result = new StringBuilder();
			for (; i < input.length; i++) { // for multiple files
				dir = terminal.getFs().validateAndReturnTheParent(input[i], terminal.getCurrent());
				String fileName = terminal.getFs().getNameOfLastElementOfThePath(input[i]);
				if (terminal.getFs().isFileAccesible(dir, fileName)) {
					result.append(Integer.toString(dir.countWords(fileName, lines)));
					result.append(" ");
					result.append(terminal.getFs().getNameOfLastElementOfThePath(input[i]));
					if (i != input.length - 1) {
						result.append("\n");
					} 
				}
				else {
					throw new AccessDeniedException(fileName);
				}
			}
			if (dir != null) {
				return result.toString();
			} else {
				throw new NoSuchFileException("");
			}
		} else {
			int counter = 0;
			if (lines) {
				for (; i < input.length; i++) {
					if (input[i].contains("\n")) {
						counter += input[i].lines().count();
					}
				}
			} else {
				for (; i < input.length; i++) {
					String[] splittedByNewLineCharacter = input[i].split("\n");
					for (int j = 0; j < splittedByNewLineCharacter.length; j++) {
						if (!splittedByNewLineCharacter[j].equals("")) {
							counter += splittedByNewLineCharacter[j].split(" ").length;
						}
					}
				}
			}
			return Integer.toString(counter);
		}
	}
}
