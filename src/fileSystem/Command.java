package fileSystem;

import java.nio.file.FileSystemException;

public interface Command {
	public String execute(String[] input) throws FileSystemException;
}
