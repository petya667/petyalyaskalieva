package fileSystem;

public class LsCommand implements Command {
	Terminal terminal;

	LsCommand(Terminal terminal) {
		this.terminal = terminal;
	}

	@Override
	public String execute(String[] input) {
		if (input.length == 1) {
			String result = new String(terminal.getCurrent().list());
			System.out.println(result);
			return result;
		} else if (input[input.length - 2].equals("--sorted") && input[input.length - 1].equals("desc")) {
			String result = new String(terminal.getCurrent().listBySize_Descending());
			System.out.println(result);
			return result;
		}
		return "";
	}
}
