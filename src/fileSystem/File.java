package fileSystem;

import java.nio.file.FileSystemException;
import java.util.HashMap;
import java.util.Map;

public class File {
	private String name;
	private int size;

	private Map<Integer, String> content;

	public File(String name) {
		this.name = name;
		size = 1;
		content = new HashMap<>();
		content.put(0, "");
	}

	public File() {
		size = 1;
		name = "NewFile";

		content = new HashMap<>();
		content.put(0, "");
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void write(int line, String contentOnOneLine, boolean overwrite) throws FileSystemException {
		if (line <= 0) {
			throw new FileSystemException(Integer.toString(line) + " wrong input ");
		}
		if (!content.containsKey(line - 1)) {
			size -= getMaxValueOfTheHashMap();
			content.put(line - 1, contentOnOneLine);
			size += getMaxValueOfTheHashMap(); // lines and after the if-else we add numChars
		} else {
			if (overwrite) {
				size -= content.get(line - 1).length();
				content.replace(line - 1, contentOnOneLine);
			} else {
				content.replace(line - 1, content.get(line - 1).concat(contentOnOneLine));
			}
		}
		size += contentOnOneLine.length();
	}

	public int getEventualIncreasingSizeOfFileAfterWriting(int line, String contentOnOneLine, boolean overwrite)
			throws FileSystemException {
		// the function doesn't add anything to the file content
		// used in write command to check if the memory is enough
		if (line <= 0) {
			throw new FileSystemException(Integer.toString(line) + " wrong input ");
		}
		int eventualSize = 0;
		if (!content.containsKey(line - 1)) {
			eventualSize -= getMaxValueOfTheHashMap();
			eventualSize += getMaxValueOfTheHashMap();
		} else {
			if (overwrite) {
				eventualSize -= content.get(line - 1).length();
			}
		}
		eventualSize += contentOnOneLine.length();
		return eventualSize;
	}

	public String[] open() {
		int size = getMaxValueOfTheHashMap() + 1;
		String[] array = new String[size]; // the last element
		for (int i = 0; i < size; i++) {
			if (content.get(i) != null) {
				array[i] = content.get(i);
			} else {
				array[i] = "";
			}
		}
		return array;
	}

	private int getMaxValueOfTheHashMap() {
		int maxEntry = 0;
		for (Map.Entry<Integer, String> entry : content.entrySet()) {
			if (maxEntry == 0 || entry.getKey() > maxEntry) {
				maxEntry = entry.getKey();
			}
		}
		return maxEntry;
	}

	public void display() {
		System.out.print(name + " ");
	}

	public int getSize() {
		return size;
	}

	public int countWords(boolean lines) { // in other class with which file
		if (lines) {
			return getMaxValueOfTheHashMap() + 1; // returns the number of lines
		}
		int counter = 0;
		for (int i = 0; i < getMaxValueOfTheHashMap() + 1; i++) { // reading line by line
			if (!content.containsKey(i) || content.get(i).equals("")) {
				continue;
			}
			counter += content.get(i).split(" ").length;
		}
		return counter;
	}

	public static void main(String[] args) throws FileSystemException {
		File obj = new File(); // 6
		obj.write(3, "hhh", false);
		String[] array = obj.open();
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
		System.out.println(obj.getSize());
		System.out.println("-");
		obj.write(1, "newtext", false); // 13
		array = obj.open();
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
		System.out.println(obj.getSize());
		System.out.println("-");
		obj.write(2, "changed", false); // 20
		array = obj.open();
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
		System.out.println(obj.getSize());
		System.out.println("-");
		obj.write(2, "changedsecondtime", false); // 37
		array = obj.open();
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
		System.out.println(obj.getSize());
	}

}
