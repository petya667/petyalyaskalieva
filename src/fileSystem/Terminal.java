package fileSystem;

import java.nio.file.AccessDeniedException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystemException;
import java.nio.file.NoSuchFileException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Terminal {
	private Map<String, Command> commands;
	private Directory current;
	private FileSystem fs;

	Terminal(Directory current, FileSystem fs) throws FileAlreadyExistsException {
		this.current = current;
		this.fs = fs;
		commands = new HashMap<>();
		commands.put("cat", new CatCommand(this));
		commands.put("cd", new CdCommand(this));
		commands.put("create_file", new CreateFileCommand(this));
		commands.put("mkdir", new MkdirCommand(this));
		commands.put("write", new WriteCommand(this));
		commands.put("ls", new LsCommand(this));
		commands.put("pipe", new PipeCommand(this));
		commands.put("wc", new WordCountCommand(this));
	}

	public void run() {
		Scanner scanner = new Scanner(System.in);
		boolean running = true;
		while (running) {
			String line = scanner.nextLine();
			String[] words = line.split(" ");
			if (words[0].equals("exit")) {
				running = false;
			} else {
				try {
					if (line.contains("|")) {
						commands.get("pipe").execute(words);
					} else if (commands.containsKey(words[0])) {
						commands.get(words[0]).execute(words);
					} else {
						throw new FileSystemException(words[0]);
					}
				} catch (FileAlreadyExistsException e) {
					System.out.println(e.getMessage() + "already exists");
				} catch (NoSuchFileException e) {
					System.out.println(e.getMessage() + " No such file or directory");
				} catch (AccessDeniedException e) {
					System.out.println(e.getMessage() + " file not accessible");
				}
				catch (FileSystemException e) {
					System.out.println(e.getMessage() + " command not found");
				}
			}
		}
		scanner.close();
	}

	public FileSystem getFs() {
		return fs;
	}

	public Map<String, Command> getCommands() {
		return commands;
	}

	public void setCommands(Map<String, Command> commands) {
		this.commands = commands;
	}

	public Directory getCurrent() {
		return current;
	}

	public void setCurrent(Directory current) {
		this.current = current;
	}

	public static void main(String[] args) throws FileSystemException {
		System.out.println("Enter:");
		FileSystem fs = new FileSystem();
		Terminal terminal = new Terminal(fs.getRoot().getSubDirectory("home"), fs);
		terminal.run();
	}

}
