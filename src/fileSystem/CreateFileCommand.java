package fileSystem;

import java.nio.file.FileSystemException;

public class CreateFileCommand implements Command {
	Terminal terminal;

	CreateFileCommand(Terminal terminal) {
		this.terminal = terminal;
	}

	@Override
	public String execute(String[] input) throws FileSystemException {
		if (input.length > 1) {
			for (int i = 1; i < input.length; i++) {
				if (!(terminal.getFs().getTotalSize() + 1 <= terminal.getFs().getMEMORY())) {
					terminal.getFs().deleteNonAccessibleFiles();
				}
				Directory dir = terminal.getFs().validateAndReturnTheParent(input[i], terminal.getCurrent());
				File f = dir.createFile(terminal.getFs().getNameOfLastElementOfThePath(input[i]));
				f.setSize(1);
				terminal.getFs().setTotalSize(terminal.getFs().getTotalSize() + 1);
			}
		} else {
			throw new FileSystemException(input[0] + " missing file operand ");
		}
		return "";
	}
}
