package fileSystem;

import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystemException;
import java.nio.file.NoSuchFileException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Directory {

	private String name;
	private Map<String, File> files;
	private Map<String, Directory> folders;
	private Directory parent;

	public Directory() { // root directory
		files = new HashMap<String, File>();
		folders = new HashMap<String, Directory>();
		folders.put("/", this);
		name = "/";
		parent = this;
	}

	public Directory(String name) {
		files = new HashMap<String, File>();
		folders = new HashMap<String, Directory>();
		this.name = name;
	}

	public Directory(String name, Directory par) {
		files = new HashMap<String, File>();
		folders = new HashMap<String, Directory>();
		this.name = name;
		parent = par;
	}

	public Directory createDirectory(String name) throws FileAlreadyExistsException { // in case with duplicating names
																						// throw exception
		if (folders.containsKey(name)) {
			throw new FileAlreadyExistsException(name);
		}
		Directory dir = new Directory(name, this);
		folders.put(name, dir);
		return dir;
	}

	public File createFile(String name) throws FileAlreadyExistsException { // create_file
		if (files.containsKey(name)) {
			throw new FileAlreadyExistsException(name);
		}
		File file = new File(name);
		files.put(name, file);
		return file;
	}

	public String cat(String fileName) throws NoSuchFileException {
		if (files.containsKey(fileName)) {
			String joined = ((String.join("\n", files.get(fileName).open())));
			return joined;
		} else {
			throw new NoSuchFileException(fileName);
		}
	}

	public String write(String fileName, int line, String contentOnOneLine, boolean overwrite)
			throws FileSystemException {
		if (files.containsKey(fileName)) {
			files.get(fileName).write(line, contentOnOneLine, overwrite);
			return "";
		} else {
			throw new NoSuchFileException(fileName);
		}
	}

	public int getEventualIncreasingSizeOfFileAfterWriting(String fileName, int line, String contentOnOneLine,
			boolean overwrite) throws FileSystemException {
		if (files.containsKey(fileName)) {
			return files.get(fileName).getEventualIncreasingSizeOfFileAfterWriting(line, contentOnOneLine, overwrite);
		} else {
			throw new NoSuchFileException(fileName);
		}
	}

	public String list() {
		String result;
		if (!files.isEmpty() && !folders.isEmpty()) {
			result = ((String.join(" ", folders.keySet())).concat(" ")).concat((String.join(" ", files.keySet())));
		} else if (folders.isEmpty() && !files.isEmpty()) {
			result = String.join(" ", files.keySet());
		} else if (!folders.isEmpty() && files.isEmpty()) {
			result = String.join(" ", folders.keySet());
		} else {
			result = "";
		}
		return result;
	}

	public String listBySize_Descending() {
		String[] arr = new String[folders.size() + files.size()];
		String[] result = new String[arr.length];
		System.arraycopy(getFoldersAndFilesSortedBySize(), 0, arr, 0, getFoldersAndFilesSortedBySize().length);
		int j = 0;
		for (int i = arr.length - 1; i >= 0; i--) { // in descending order
			result[j] = arr[i];
			j++;
		}
		String joined = ((String.join(" ", result)));
		return joined;
	}

	private String[] getFoldersAndFilesSortedBySize() {
		int[] result = new int[folders.size() + files.size()];
		String[] keys = new String[folders.size() + files.size()];
		int i = 0;
		for (HashMap.Entry<String, Directory> entry : folders.entrySet()) {
			result[i] = entry.getValue().getSizeOfFilesInDirectory();
			keys[i] = entry.getKey();
			i++;
		}
		for (HashMap.Entry<String, File> entry : files.entrySet()) {
			result[i] = entry.getValue().getSize();
			keys[i] = entry.getKey();
			i++;
		}
		// sorting function which by sorting the size in results, arranges the strings
		String[] sortedStrings = new String[keys.length];
		sortedStrings = arrangingStringsByTheSizeOfTheirObjects(result, keys);
		return sortedStrings;
	}

	private int getSizeOfFilesInDirectory() {
		int result = 0;
		for (HashMap.Entry<String, File> entry : files.entrySet()) {
			result += entry.getValue().getSize();
		}
		return result;
	}

	public Directory getParent() {
		return parent;
	}

	public void setParent(Directory parent) {
		this.parent = parent;
	}

	private String[] arrangingStringsByTheSizeOfTheirObjects(int[] result, String[] keys) {
		int n = result.length;
		int tmp;
		String temp;
		for (int i = 0; i < n - 1; i++) {
			for (int j = 0; j < n - i - 1; j++)
				if (result[j] > result[j + 1]) {
					tmp = result[j];
					result[j] = result[j + 1];
					result[j + 1] = tmp;
					temp = new String(keys[j]);
					keys[j] = keys[j + 1];
					keys[j + 1] = temp;
				}
		}
		return keys;
	}

	public String getName() {
		return name;
	}

	public boolean contains(String subdirName) {
		if (folders.containsKey(subdirName)) {
			return true;
		}
		return false;
	}

	public Directory getSubDirectory(String dirName) throws NoSuchFileException {
		if (contains(dirName)) {
			return folders.get(dirName);
		} else {
			throw new NoSuchFileException(dirName);
		}
	}

	public Directory getOneStepCloseDirectory(String input) throws NoSuchFileException {
		if (input.equals(".")) {
			return this;
		} else if (input.equals("..")) {
			return getParent();
		} else {
			return getSubDirectory(input);
		}
	}

	public int countWords(String fileName, boolean lines) throws NoSuchFileException {
		if (files.containsKey(fileName)) {
			return files.get(fileName).countWords(lines);
		} else {
			throw new NoSuchFileException(fileName);
		}
	}

	public void deleteFile(String fileName) throws NoSuchFileException {
		if (files.containsKey(fileName)) {
			files.remove(fileName);
		} else {
			throw new NoSuchFileException(fileName);
		}
	}

	public static void main(String[] args) throws FileSystemException {
		Directory obj = new Directory();
		obj.createFile("file1");
		obj.write("file1", 3, "hdnhf", false); // size 8
		obj.createFile("file2");
		obj.createFile("file3");
		obj.write("file2", 5, "nnbhhhf", false);
		obj.createDirectory("dir1");
		System.out.println(obj.listBySize_Descending());
		// do we have to count home
		Scanner sc = new Scanner(System.in);
		String path = sc.nextLine();
		System.out.println(path.substring(1).split("/")[0].equals("")); // / given
		sc.close();

	}

}
