package fileSystem;

import java.nio.file.FileSystemException;

public class MkdirCommand implements Command {
	Terminal terminal;

	MkdirCommand(Terminal terminal) {
		this.terminal = terminal;
	}

	@Override
	public String execute(String[] input) throws FileSystemException {
		if (input.length > 1 && !input[1].equals("..") && !input[1].equals(".")) {
			for (int i = 1; i < input.length; i++) {
				Directory dir = terminal.getFs().validateAndReturnTheParent(input[i], terminal.getCurrent());
				dir.createDirectory(terminal.getFs().getNameOfLastElementOfThePath(input[i]));
			}
		} else {
			throw new FileSystemException(input[0] + " missing or wrong file operand ");
		}
		return "";
	}
}
